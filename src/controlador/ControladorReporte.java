/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.fichadas.ReporteExcel;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import modelo.Feriado;
import modelo.LicenciaEmpleado;
import modelo.Userinfo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;
import reporte.detalleEmpleado;
import reporte.Marcadas;
import reporte.ReporteFichada;
import reporte.ResumenFichadas;

/**
 *
 * @author Viktor
 */
public class ControladorReporte {

    public ControladorReporte() {
    }

    public void crearReportes(List<List<Marcadas>> listaMes, List<Userinfo> listaEmpleados, List<String[]> listaResumen, String periodo, String ruta, List<Feriado> feriados, int mes, int ano) throws IOException {

        ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();
        int index = 0;
        JasperPrint print = null;
        List<detalleEmpleado> listaEmpleados2 = new ArrayList<>();

        try {
            for (Userinfo e : listaEmpleados) {

                List<LicenciaEmpleado> licencias = contLicenciaEmpleado.buscar(e, mes, ano);

                List<Marcadas> l = listaMes.get(index);
                String[] v = listaResumen.get(index);
                index++;

                //DIAS NO LABORABLES: FERIADOS, ASUETAS, HUELGAS ETC
                String feriadoString = "Días de no actividad:\n\n";
                for (Feriado f : feriados) {
                    feriadoString = feriadoString + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(f.getFecha().getTime())) + " " + f.getDetalle() + "\n";
                }

                //LICENCIAS DEL EMPLEADO
                String licenciasEmp = "Licencias del mes:\n\n";
                for (LicenciaEmpleado le : licencias) {
                    licenciasEmp = licenciasEmp + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_inicio().getTime())) + " - " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_fin().getTime())) + " " + le.getDetalle() + "\n";
                }

                detalleEmpleado emp = new detalleEmpleado();
                emp.setNombre(e.getName());
                emp.setCuil("" + e.getUserid());
                emp.setPeriodo(periodo);
                emp.setNormales(v[0]);
                emp.setExtras(v[1]);
                emp.setRetrazo(v[2]);
                emp.setSabadoM(v[3]);
                emp.setSabadoT(v[4]);
                emp.setDomingo(v[5]);
                emp.setFeriado(v[6]);
                emp.setFeriados(feriadoString);
                emp.setLicencias(licenciasEmp);
                emp.setReloj("");
                emp.setHorario(new ContPersonaGrupo().traerHorario(e));
                emp.setMarcadas(l);
                listaEmpleados2.add(emp);
            }

            // REPORTE
            Map map = new HashMap();
            map.put("subreporte_dir", "reporte/");
            print = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reporte/detalleEmpleado.jasper"),
                    map, new JRBeanCollectionDataSource(listaEmpleados2));

            // con JFILECHOOSER
            javax.swing.JFileChooser jF1 = new javax.swing.JFileChooser();
            String rutaRep = "";
            try {
                if (jF1.showSaveDialog(null) == jF1.APPROVE_OPTION) {
                    Calendar c = Calendar.getInstance();
                    rutaRep = jF1.getSelectedFile().getAbsolutePath();
                    File f = new File(rutaRep + ".pdf");
                    if (f.exists()) {
                        if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, "El fichero existe,deseas reemplazarlo?", "Titulo", JOptionPane.YES_NO_OPTION)) //Has aceptado...has lo kieras...... 
                        {
                            if (f.delete()) {
                                JasperExportManager.exportReportToPdfFile(print, rutaRep + ".pdf");
                            } else {
                                JOptionPane.showMessageDialog(null, "El archivo existenete no se puede eliminar.", "", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    } else {
                        JasperExportManager.exportReportToPdfFile(print, rutaRep + ".pdf");
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (JRException ex) {
            Logger.getLogger(ControladorReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void imprimirMuchosReportes(List<List<Marcadas>> listaMes, List<Userinfo> listaEmpleados, List<String[]> listaResumen, String periodo, String ruta, List<Feriado> feriados, int mes, int ano) throws IOException {

        ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();
        int index = 0;
        JasperPrint print = null;
        List<detalleEmpleado> listaEmpleados2 = new ArrayList<>();

        for (Userinfo e : listaEmpleados) {

            List<LicenciaEmpleado> licencias = contLicenciaEmpleado.buscar(e, mes, ano);

            List<Marcadas> l = listaMes.get(index);
            String[] v = listaResumen.get(index);

            //DIAS NO LABORABLES: FERIADOS, ASUETAS, HUELGAS ETC
            String feriadoString = "Días de no actividad:\n\n";
            for (Feriado f : feriados) {
                feriadoString = feriadoString + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(f.getFecha().getTime())) + " " + f.getDetalle() + "\n";
            }

            //LICENCIAS DEL EMPLEADO
            String licenciasEmp = "Licencias del mes:\n\n";
            for (LicenciaEmpleado le : licencias) {
                licenciasEmp = licenciasEmp + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_inicio().getTime())) + " - " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_fin().getTime())) + " " + le.getDetalle() + "\n";
            }

            detalleEmpleado emp = new detalleEmpleado();
            emp.setNombre(e.getName());
            emp.setCuil("" + e.getUserid());
            emp.setPeriodo(periodo);
            emp.setNormales(v[0]);
            emp.setExtras(v[1]);
            emp.setRetrazo(v[2]);
            emp.setSabadoM(v[3]);
            emp.setSabadoT(v[4]);
            emp.setDomingo(v[5]);
            emp.setFeriado(v[6]);
            emp.setFeriados(feriadoString);
            emp.setLicencias(licenciasEmp);
            emp.setReloj("");
            emp.setHorario(new ContPersonaGrupo().traerHorario(e));
            emp.setMarcadas(l);
            listaEmpleados2.add(emp);

            index++;

        }
        try {
            //
            Map map = new HashMap();
            map.put("subreporte_dir", "reporte/marcadas.jasper");
            print = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reporte/detalleEmpleado.jasper"),
                    map, new JRBeanCollectionDataSource(listaEmpleados2));

            JasperPrintManager.printReport(print, true);

        } catch (JRException ex) {
            Logger.getLogger(ControladorReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void crearReportes(List<ReporteFichada> listaRF, Userinfo e, String[] resumen, String periodo, List<Feriado> feriados, String origenFichada, List<LicenciaEmpleado> listaLicencias) {
//        
        ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();
        //DIAS NO LABORABLES: FERIADOS, ASUETAS, HUELGAS ETC
        String feriadoString = "Dias de no actividad:\n\n";
        for (Feriado f : feriados) {
            feriadoString = feriadoString + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(f.getFecha().getTime())) + " " + f.getDetalle() + "\n";
        }

        //LICENCIAS DEL EMPLEADO
        String licenciasEmp = "Licencias del mes:\n\n";
        for (LicenciaEmpleado le : listaLicencias) {
            licenciasEmp = licenciasEmp + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_inicio().getTime())) + " - " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_fin().getTime())) + " " + le.getDetalle() + "\n";
        }

        try {
            Map map = new HashMap();
            JasperPrint print;
            JDialog reporte = new JDialog();
            reporte.setSize(850, 700);
            reporte.setModal(true);
            reporte.setTitle("Fichada");
            map.put("nombre", e.getName());
            map.put("cuil", e.getUserid());
            map.put("periodo", periodo);
            map.put("total_normales", resumen[0]);
            map.put("total_extra", resumen[1]);
            map.put("total_retrazo", resumen[2]);
            map.put("sabadoManana", resumen[3]);
            map.put("sabadoTarde", resumen[4]);
            map.put("domingo", resumen[5]);
            map.put("feriado", resumen[6]);
            map.put("detalles", feriadoString);
            map.put("licencias", licenciasEmp);
            map.put("origen", origenFichada);
            map.put("horario", new ContPersonaGrupo().traerHorario(e));
            print = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reporte/reporteFichadas.jasper"),
                    map, new JRBeanCollectionDataSource(listaRF));

            JRViewer jv = new JRViewer(print);

            reporte.getContentPane().add(jv);

            reporte.setVisible(true);

            reporte.addComponentListener(new java.awt.event.ComponentAdapter() {
                private void setAlwaysOnTop() {

                    setAlwaysOnTop();
                }
            });

        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, "Error: \n" + ex, "Error", JOptionPane.ERROR_MESSAGE);

            ex.printStackTrace();
        }

    }

    public void imprimirReporteFichada(List<ReporteFichada> listaRF, Userinfo e, String[] resumen, String periodo, List<Feriado> feriados, String origenFichada, List<LicenciaEmpleado> listaLicencias) {

        JasperReport reporte;
        JasperPrint reporte_view;

        ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();
        //DIAS NO LABORABLES: FERIADOS, ASUETAS, HUELGAS ETC
        String feriadoString = "Dias de no actividad:\n\n";
        for (Feriado f : feriados) {
            feriadoString = feriadoString + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(f.getFecha().getTime())) + " " + f.getDetalle() + "\n";
        }

        //LICENCIAS DEL EMPLEADO
        String licenciasEmp = "Licencias del mes:\n\n";
        for (LicenciaEmpleado le : listaLicencias) {
            licenciasEmp = licenciasEmp + " " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_inicio().getTime())) + " - " + contLicenciaEmpleado.retornarFecha(new Timestamp(le.getFecha_fin().getTime())) + " " + le.getDetalle() + "\n";
        }

        try {
//direccion del archivo JASPER
            URL in = this.getClass().getResource("/reporte/reporteFichadas.jasper");
            reporte = (JasperReport) JRLoader.loadObject(in);
//Se crea un objeto HashMap

            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();

//parametros de entrada
            parametros.put("nombre", e.getName());
            parametros.put("cuil", e.getUserid());
            parametros.put("periodo", periodo);
            parametros.put("total_normales", resumen[0]);
            parametros.put("total_extra", resumen[1]);
            parametros.put("total_retrazo", resumen[2]);
            parametros.put("sabadoManana", resumen[3]);
            parametros.put("sabadoTarde", resumen[4]);
            parametros.put("domingo", resumen[5]);
            parametros.put("feriado", resumen[6]);
            parametros.put("detalles", feriadoString);
            parametros.put("licencias", licenciasEmp);
            parametros.put("origen", origenFichada);
            parametros.put("horario", new ContPersonaGrupo().traerHorario(e));
//-----------------------------------
            reporte_view = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(listaRF));

            JasperPrintManager.printReport(reporte_view, true);

        } catch (Exception E) {
            E.printStackTrace();
        }
    }

    public void crearReporteResumenes(List<ResumenFichadas> lista, String periodo) {
        try {
            Map map = new HashMap();
            JasperPrint print;
            JDialog reporte = new JDialog();
            reporte.setSize(850, 700);
            reporte.setModal(true);
            reporte.setTitle("Resumen de Horas");
            map.put("periodo", periodo);
            print = JasperFillManager.fillReport(this.getClass().getClassLoader().getResourceAsStream("reporte/resumenFichadas.jasper"),
                    map, new JRBeanCollectionDataSource(lista));

            JRViewer jv = new JRViewer(print);
            reporte.getContentPane().add(jv);

            reporte.setVisible(true);

            reporte.addComponentListener(new java.awt.event.ComponentAdapter() {
                private void setAlwaysOnTop() {

                    setAlwaysOnTop();
                }
            });
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, "Error: \n" + ex, "Error", JOptionPane.ERROR_MESSAGE);

            ex.printStackTrace();
        }
    }

}
