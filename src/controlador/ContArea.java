/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.List;
import modelo.Area;

/**
 *
 * @author Viktor
 */
public class ContArea extends ControladorGeneral{

    public ContArea() {
        super();
    }
    
    private Area setearObjeto (List lista){
        return new Area(Integer.parseInt(lista.get(0).toString()), lista.get(1).toString());
    }
    
    public List<Area> encontrarTodos(){
        List<Area> listaRetornar = new ArrayList<>();
        List<List> lista = dameListaCompleta(columnasAreas, consultaAreas, conexionRelojLocal);
        
        for (List lista1 : lista) {
            listaRetornar.add(setearObjeto(lista1));
        }
        
        return listaRetornar;
    }
    
    public Area encontrar(int id){
        Area areaRetornar =  null;
        Object [] datos =  {id};
        
        List<List> lista = buscarPorParametro(columnasAreas, consultaAreaID, datos, conexionRelojLocal);
        for (List lista1 : lista) {
            areaRetornar = setearObjeto(lista1);
        }
        
        return areaRetornar;
    }
    
    public int insert(Area a){
        Object[] datos = {a.getDetalle()};
        return ejecutarSentencia(datos, areaINSERT, conexionRelojLocal);
    }
    
    public int update(Area a){
        Object[] datos = {a.getDetalle(),a.getId()};
        return  ejecutarSentencia(datos, areaUPDATE, conexionRelojLocal);
    }
}
