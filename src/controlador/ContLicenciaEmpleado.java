/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import controlador.fichadas.Utilidades;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import modelo.LicenciaEmpleado;
import modelo.Userinfo;


/**
 *
 * @author Viktor
 */
public class ContLicenciaEmpleado extends ControladorGeneral{

    public ContLicenciaEmpleado() {
    }
    
    private LicenciaEmpleado setearObjeto(List lista){
        return new LicenciaEmpleado(Integer.parseInt(lista.get(0).toString()), Date.valueOf(lista.get(1).toString()), Date.valueOf(lista.get(2).toString()),
                Integer.parseInt(lista.get(3).toString()), Integer.parseInt(lista.get(4).toString()), lista.get(5).toString(), lista.get(6).toString(),Boolean.parseBoolean(lista.get(7).toString()),
                lista.get(8).toString(),lista.get(9).toString(),lista.get(10).toString(),Timestamp.valueOf(lista.get(11).toString()));
    }

    public void create(LicenciaEmpleado licenciaEmpleado) {
        Object[] arreglo = {licenciaEmpleado.getFecha_fin(),licenciaEmpleado.getFecha_inicio(),licenciaEmpleado.getUserinfoid(),licenciaEmpleado.getLicencia(),licenciaEmpleado.getDetalle(),licenciaEmpleado.getTerminal(),licenciaEmpleado.getIp(),licenciaEmpleado.getUsuariosistema(),licenciaEmpleado.getFechaauditoria()};
        
        ejecutarSentencia(arreglo, BDSentencias.licenciaEmpleadoInsert, BDSentencias.conexionRelojLocal);
    }
    
    public List<LicenciaEmpleado> buscar(Userinfo e) {
        List<LicenciaEmpleado> lista = new ArrayList<>();
        
        Object [] parametros = {e.getId()};
        
        for(List l2: buscarPorParametro(BDSentencias.empleadolicecniaColumnas, BDSentencias.empleadoLicenciaTodoID, parametros, BDSentencias.conexionRelojLocal)){
                
                if(l2.get(8) == null)
                    l2.set(8, "");
                
                if(l2.get(9) == null)
                    l2.set(9, "");
                
                if(l2.get(10) == null)
                    l2.set(10, "");
                
                if(l2.get(11) == null)
                    l2.set(11, new Timestamp(Calendar.getInstance().getTimeInMillis()));
            
                LicenciaEmpleado le = setearObjeto(l2);
                
                lista.add(le);
            
        }
        
        return lista;
    }
    public List<LicenciaEmpleado> buscar(int userinfoid) {
        List<LicenciaEmpleado> lista = new ArrayList<>();
        
        Object [] parametros = {userinfoid};
        
        for(List l2: buscarPorParametro(BDSentencias.empleadolicecniaColumnas, BDSentencias.empleadoLicenciaTodoID, parametros, BDSentencias.conexionRelojLocal)){
                
                if(l2.get(8) == null)
                    l2.set(8, "");
                
                if(l2.get(9) == null)
                    l2.set(9, "");
                
                if(l2.get(10) == null)
                    l2.set(10, "");
                
                if(l2.get(11) == null)
                    l2.set(11, new Timestamp(Calendar.getInstance().getTimeInMillis()));
            
                LicenciaEmpleado le = setearObjeto(l2);
                
                lista.add(le);
            
        }
        
        return lista;
    }
    
    public List<LicenciaEmpleado> buscar(Userinfo e, int mes, int ano ) throws IOException {
        List<LicenciaEmpleado> lista = new ArrayList<>();
        
        Utilidades u = new Utilidades();
        List<String> fechas = u.fechas(ano, mes);
        Date f1 = Date.valueOf(fechas.get(0)) ;//traigo el primero
        Date f2 =  Date.valueOf(fechas.get(fechas.size()-1));//traigo el ultimo
        
        Object [] parametros = {e.getId(),f1,f2};
        
        for(List l2: buscarPorParametro(BDSentencias.empleadolicecniaColumnas, BDSentencias.consultaLicenciasDeMes, parametros, BDSentencias.conexionRelojLocal)){
            
                
                if(l2.get(8) == null)
                    l2.set(8, "");
                
                if(l2.get(9) == null)
                    l2.set(9, "");
                
                if(l2.get(10) == null)
                    l2.set(10, "");
                
                if(l2.get(11) == null)
                    l2.set(11, new Timestamp(Calendar.getInstance().getTimeInMillis()));
            
                
                LicenciaEmpleado le = setearObjeto(l2);
                
                lista.add(le);
            
        }
        
        return lista;
    }

    public void update(LicenciaEmpleado licCortar) {
        Object[] datos = {licCortar.getFecha_fin(),licCortar.getTerminal(),licCortar.getIp(),licCortar.getUsuariosistema(),licCortar.getFechaauditoria(), licCortar.getId()};
        ejecutarSentencia(datos, BDSentencias.updateEmpleadoLic, BDSentencias.conexionRelojLocal);
    }
}
