/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author Viktor
 */
public class BaseDatasource {
    /** Pool de conexiones */
    public DataSource dataSource;
    BasicDataSource basicDataSource = new BasicDataSource();
    
    private String puerto = "3307";
    
    public BaseDatasource() {
        /** Inicializacion de BasicDataSource */
        

        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUsername("root");
        basicDataSource.setPassword("");
        basicDataSource.setUrl("jdbc:mysql://localhost:"+puerto+"/reloj_muni");

        // Opcional. Sentencia SQL que le puede servir a BasicDataSource
        // para comprobar que la conexion es correcta.
        basicDataSource.setValidationQuery("select 1");

        dataSource = basicDataSource;
    }
    
//    public void estableceConexion(String[] datosConexion){
//        /** Inicializacion de BasicDataSource */
//        BasicDataSource basicDataSource = new BasicDataSource();
//
//        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
//        basicDataSource.setUsername(datosConexion[1]);
//        basicDataSource.setPassword(datosConexion[2]);
//        basicDataSource.setUrl("jdbc:mysql://"+datosConexion[3]+":"+puerto+"/"+datosConexion[0]);
//
//        // Opcional. Sentencia SQL que le puede servir a BasicDataSource
//        // para comprobar que la conexion es correcta.
//        basicDataSource.setValidationQuery("select 1");
//
//        dataSource = basicDataSource;
//    }
    
    public Connection estableceConexion(Connection c,String[] datosConexion) throws SQLException{
        /** Inicializacion de BasicDataSource */

        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUsername(datosConexion[1]);
        basicDataSource.setPassword(datosConexion[2]);
        basicDataSource.setUrl("jdbc:mysql://"+datosConexion[3]+":"+puerto+"/"+datosConexion[0]);

        // Opcional. Sentencia SQL que le puede servir a BasicDataSource
        // para comprobar que la conexion es correcta.
        basicDataSource.setValidationQuery("select 1");

        dataSource = basicDataSource;
        
        return dataSource.getConnection();
    }
    
    public void cierraConexion(Connection conexion) {
        try {
            if (null != conexion) {
                // En realidad no cierra, solo libera la conexion.
                conexion.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
