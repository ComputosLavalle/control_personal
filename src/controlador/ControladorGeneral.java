/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Fichada;
import modelo.LicenciaEmpleado;

/**
 *
 * @author UPID
 */
public class ControladorGeneral implements BDSentencias {

    protected BaseDeDatos bd = new BaseDeDatos();
//    private BaseDatasource bd = new BaseDatasource();
    Connection conexion = null;

    /**
     *
     *
     * @param arreglosDatos
     * @param consulta
     */
    public int ejecutarSentencia(Object[] arreglosDatos, String consulta, String[] datosConexion) {
//        Connection conexion = null;
        int id = 0;
        try {
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,datosConexion);
            PreparedStatement pe = conexion.prepareStatement(consulta);

            for (int i = 0; i < arreglosDatos.length; i++) {
                pe.setObject(i + 1, arreglosDatos[i]);
            }
//            System.out.println("sentencia: "+pe.toString());
            id = pe.executeUpdate();
            //System.err.println("ID_RESULT: "+id);
        } catch (Exception e) {
            System.out.println("Problemas al ejecutarSentencia " + e.getMessage());
            e.printStackTrace();
        } finally {
            bd.cierraConexion(conexion);
        }
        return id;
    }

    public void ficharAloLoco(List<Fichada> fichadas, String[] datosConexion) {
        Connection conexion = null;
        
        
        try {
            // para el progressbar
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,datosConexion);
            for (Fichada fichada : fichadas) {
                if (!existeEnBD(fichada)) {
                    PreparedStatement pe = conexion.prepareStatement(BDSentencias.fichadaInsert);

                    pe.setLong(1, fichada.getUserinfoid());
                    pe.setDate(2, fichada.getFecha());
                    pe.setTime(3, fichada.getHora());
                    pe.setString(4, fichada.getReloj());
                    

                    pe.executeUpdate();
                }
                
            }

        } catch (Exception e) {
            System.out.println("Problemas al dameListaCompleta " + e.getMessage());
            e.printStackTrace();
        } finally {
            bd.cierraConexion(conexion);
        }
    }

    public List<List> dameListaCompleta(String[] arregloColumnas, String consulta, String[] datosConexion) {
        List<List> listaCompleta = new ArrayList();

////        Connection conexion = null;
        try {
////            conexion = bd.estableceConexion(datosConexion);
//            System.err.println(consulta);
//            System.err.println(arregloColumnas.toString());
            conexion = bd.estableceConexion(conexion,datosConexion);
            PreparedStatement pe = conexion.prepareStatement(consulta);

            ResultSet rs = pe.executeQuery();
            while (rs.next()) {
                List objeto = new ArrayList();
                for (int i = 0; i < arregloColumnas.length; i++) {
                    objeto.add(rs.getObject(arregloColumnas[i]));
                }
                listaCompleta.add(objeto);

            }

        } catch (Exception e) {
            System.out.println("Problemas al dameListaCompleta " + e.getMessage());
            e.printStackTrace();
        } finally {
            bd.cierraConexion(conexion);
        }

        return listaCompleta;
    }

    public List<List> buscarPorParametro(String[] arregloColumnas, String consulta, Object[] parametros, String[] datosConexion) {
        List listaCompleto = new ArrayList();

        try {
            conexion = bd.estableceConexion(conexion,datosConexion);
            PreparedStatement pe = conexion.prepareStatement(consulta);

            for (int i = 0; i < parametros.length; i++) {
                pe.setObject(i + 1, parametros[i]);
            }
//            System.err.println(pe.toString());
            ResultSet rs = pe.executeQuery();
//            System.out.println(pe.toString());

            while (rs.next()) {
                List registro = new ArrayList();
                for (int i = 0; i < arregloColumnas.length; i++) {
                    registro.add(rs.getObject(arregloColumnas[i]));
                }
                listaCompleto.add(registro);
            }

        } catch (Exception e) {
            System.out.println("problemas al buscarPorParametro - contGeneral" + e.getMessage());
            e.printStackTrace();
        } finally {
            bd.cierraConexion(conexion);
        }

        return listaCompleto;

    }

    public List buscarPorId(String[] arregloColumnas, String consulta, Object parametro, String[] datosConexion) {
        List listaCompleto = new ArrayList();

//        Connection conexion = null;
        try {
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,datosConexion);
            PreparedStatement pe = conexion.prepareStatement(consulta);

            pe.setObject(1, parametro);

            ResultSet rs = pe.executeQuery();
//            System.out.println("consulta : " + pe.toString());
            while (rs.next()) {

                for (int i = 0; i < arregloColumnas.length; i++) {
                    listaCompleto.add(rs.getObject(arregloColumnas[i]));
                }
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        } finally {
            bd.cierraConexion(conexion);
        }

        return listaCompleto;

    }

    public Timestamp getFechaServidor(String[] datosConexion) {

//        Connection conexion = null;
        Timestamp fechaServidor = null;

        try {
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,datosConexion);

            PreparedStatement pe = conexion.prepareStatement("SELECT CURRENT_TIMESTAMP");

            ResultSet rs = pe.executeQuery();

            while (rs.next()) {
                fechaServidor = rs.getTimestamp("CURRENT_TIMESTAMP");
            }

        } catch (Exception e) {
            System.out.println("Problemas al getFechaServior - controlador" + e.getMessage() + e.getStackTrace());
        } finally {
            bd.cierraConexion(conexion);
        }
        return fechaServidor;

    }

    //victor
    public List listarTodo(String[] arregloColumnas, String consulta, String[] datosConexion) {
        List listaCompleto = new ArrayList();

//        Connection conexion = null;
        try {
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,datosConexion);
            PreparedStatement pe = conexion.prepareStatement(consulta);

            ResultSet rs = pe.executeQuery();
//            System.out.println("consulta : " + pe.toString());
            while (rs.next()) {

                for (int i = 0; i < arregloColumnas.length; i++) {
                    listaCompleto.add(rs.getObject(arregloColumnas[i]));
                }
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        } finally {
            bd.cierraConexion(conexion);
        }

        return listaCompleto;
    }

    private boolean existeEnBD(Fichada fichada) {
        boolean existe = false;
//        Connection conexion = null;
        try {
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,BDSentencias.conexionRelojLocal);
            PreparedStatement pe = conexion.prepareStatement(BDSentencias.consultaExisteFichada);
            pe.setObject(1, fichada.getUserinfoid());
            pe.setObject(2, fichada.getFecha());
            pe.setObject(3, fichada.getHora());
            ResultSet rs = pe.executeQuery();
            while (rs.next()) {
                existe = true;
            }

        } catch (Exception e) {
            System.out.println("Problemas " + e.getMessage());
            e.printStackTrace();
        } finally {
            bd.cierraConexion(conexion);
        }
        
        return existe;
    }

    
    //////////////////// viktor //////////
    public String retornarFechaHora(Timestamp t){
        
        DateFormat df = DateFormat.getDateTimeInstance();
        
        return df.format(t);
    }
    
    public String retornarFecha(Timestamp t){
        
        DateFormat df = DateFormat.getDateInstance();
        
        return df.format(t);
    }
    
    public String retornarHora(Timestamp t){
        
        DateFormat df = DateFormat.getTimeInstance();
        
        return df.format(t);
    }
    
    public Timestamp getFechaServidor() {

        Connection conexion = null;
        Timestamp fechaServidor = null;

        try {
            conexion = bd.estableceConexion(conexion, conexionRelojLocal);

            PreparedStatement pe = conexion.prepareStatement("SELECT CURRENT_TIMESTAMP");

            ResultSet rs = pe.executeQuery();

            while (rs.next()) {
                fechaServidor = rs.getTimestamp("CURRENT_TIMESTAMP");
            }

        } catch (Exception e) {
            System.out.println("Problemas al getFechaServior - controlador" + e.getMessage() + e.getStackTrace());
        } finally {
            bd.cierraConexion(conexion);

        }
        return fechaServidor;

    }

    public String getNombreTerminal() {

        String nombre = null;

        try {
            InetAddress localHost = InetAddress.getLocalHost();

            nombre = localHost.getHostName();

        } catch (UnknownHostException e) {
            System.out.println("problemas al getNombreTerminal  " + e.getMessage());
        }

        return nombre;
    }
    
    public String getIpTerminal(){
        String ip = "";
    
        try {
            ip = InetAddress.getLocalHost()
                    .getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ip;
    }
    
    public void ejecutarConRollback(String[] sentencias, List<Object[]> variables, String[] datosConexion) {

        Connection conexion = null;
        try {
            conexion = bd.estableceConexion(conexion, datosConexion);

            conexion.setAutoCommit(false);

            for (int i = 0; i < sentencias.length; i++) {
                PreparedStatement pe = conexion.prepareStatement(sentencias[i]);

                for (int j = 0; j < variables.get(i).length; j++) {
                    pe.setObject(j + 1, variables.get(i)[j]);

                }
                pe.executeUpdate();
            }

            conexion.commit();

        } catch (Exception e) {
            System.out.println("problemas al ejecutar con rollback" + e.getMessage());
            JOptionPane.showMessageDialog(null, "Problemas graves\nComunicarse con el Área de Cómputos");
            e.printStackTrace();
            try {

                conexion.rollback();
            } catch (SQLException ex) {
                System.out.println("problemas al roll back " + ex);
            }
        } finally {
            bd.cierraConexion(conexion);
        }

    }
    
    public void update(LicenciaEmpleado licCortar) {
        Object[] datos = {licCortar.getFecha_fin(),licCortar.getTerminal(),licCortar.getIp(),licCortar.getUsuariosistema(),licCortar.getFechaauditoria(), licCortar.getId()};
        ejecutarSentencia(datos, BDSentencias.updateEmpleadoLic, BDSentencias.conexionRelojLocal);
    }
    
    public void delete(LicenciaEmpleado lic){
        Object[] datos = {lic.getTerminal(),lic.getIp(),lic.getUsuariosistema(),lic.getFechaauditoria(), lic.getId()};
       ejecutarSentencia(datos,  BDSentencias.deleteEmpleadoLic, BDSentencias.conexionRelojLocal);
        
    }
   

    
}
