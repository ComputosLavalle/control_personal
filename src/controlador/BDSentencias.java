/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author Viktor
 */
public interface BDSentencias {

    
    public static final String[] conexionProveedores = {"contpres", "Usuario", "123456", "192.168.1.201"};
    public static final String[] conexionInfogov = {"infogov", "Usuario", "123456", "192.168.1.201"};
    public static final String[] conexionPersonal = {"personal", "Usuario", "123456", "192.168.1.201"};
    public static final String[] conexionRelojLocal = {"reloj_muni3", "Usuario", "123456", "192.168.1.201"};
    public static final String[] conexionCentral = {"Central", "Usuario", "123456", "192.168.1.201"};

//    public static final String[] conexionRelojLocal = {"reloj_muni3", "root", "", "192.168.1.149"};
//    public static final String[] conexionProveedores = {"contpres", "root", "", "192.168.1.149"};
//    public static final String[] conexionInfogov = {"infogov", "root", "", "192.168.1.149"};
//    public static final String[] conexionPersonal = {"personal", "root", "", "192.168.1.149"};
//    public static final String[] conexionCentral = {"central", "root", "", "192.168.1.149"};
    //COLUMNAS//
    public static final String[] columnasValidar = {"Error"};
    public static final String[] columnasUsuario_oficina = {"id", "id_oficina", "id_usuario"};
    public static final String[] fichadaColumnas = { "USERINFOID", "FECHA", "FICHADAPORSISTEMA", "ESTADO", "HORA", "OBSERVACION", "RELOJ"};
    public static final String[] fichadaColumnasBasic = {"USERINFOID", "FECHA", "HORA"};
    public static final String[] fichadaColCUCUPERS = {"USERINFOID"};
    public static final String[] proveedoresColumnas = {"CucuPers"};
    public static final String[] legajoColumnas = {"CucuPers"};
    public static final String[] personaInfogovColumnas = {"CucuPers", "DetaPers"};
    public static final String[] grupoColumnas = {"ID", "NOMBRE", "ENTRADA", "SALIDA", "HORADEALMUERZO", "JORNADAEXTENDIDA"};
    public static final String[] personaGrupoColumnas = {"ID", "USERINFOID", "GRUPO_ID"};
    public static final String[] licenciaColumnas = {"ID", "DESCUENTO", "DETALLE"};
    public static final String[] empleadolicecniaColumnas = {"ID", "FECHA_FIN", "FECHA_INICIO", "USERINFOID", "LICENCIA_ID", "DETALLE", "USUARIO", "JUSTIFICACIONFALTA","TERMINAL","IP","USUARIOSISTEMA","FECHAAUDITORIA"};
    public static final String[] feriadoColumnas = {"ID", "FECHA", "DETALLE"};
    public static final String[] legajoFuncColumnas = {"FuncLega"};
    public static final String[] proveedoresFuncColumnas = {"DetaProv"};
    public static final String[] colEmpleadoColumna = {"cucupers", "detapers", "funcLega"};
    public static final String[] colEmpleadoColumna2 = {"cucupers", "detapers", "DetaProv"};
    public static final String[] coluUSERINFO = {"ID", "USERID", "NAME", "CUCUPERS", "RELOJ", "ID_SECCION","TERMINAL","IP","USUARIOSISTEMA","FECHAAUDITORIA"};
    public static final String[] columnasUsuariosDeSistema = {"ID", "USUARIO", "FECHAMODIFICACION", "USERALTA", "TIPO", "ID_AREA", "ID_SECCION", "BAJA"};
    public static final String[] columnasAreas = {"ID", "DETALLE"};
    public static final String[] columnasSecciones = {"ID", "DETALLE", "ID_AREA"};
    public static final String[] columnasReloj = {"ID", "RELOJ", "IP", "ESTADO"};
    public static final String[] columnasPathReloj = {"PATHRELOJ"};
    public static final String[] columnasLoggs = {"ID", "FECHA", "LUGARFALLA", "NOMBREDELTERMINAL", "DETALLES", "NROERROR"};

    // SELECTS //
    public static final String legajoBuscarTodoss = "SELECT * FROM legajo";
    public static final String proveedoresBuscarTodoss = "SELECT * FROM proveedores";
    public static final String cuilsEmpleados = "SELECT DISTINCT `USERINFOID` FROM `reloj_muni`.`fichada`";
    public static final String nombrePersonasTodos = "SELECT `CucuPers`, `DetaPers` FROM `infogov`.`persona`";
    public static final String nombreEmpleado = "SELECT `CucuPers`, `DetaPers` FROM `infogov`.`persona` WHERE CucuPers = ?";
    public static final String fichadaMesTodas = "SELECT USERINFOID,FECHA,HORA FROM fichada WHERE FECHA >= ? AND HORA > '00:00:00' ORDER BY FECHA,HORA ASC";
    public static final String fichadaMes = "SELECT USERINFOID,FECHA,FICHADAPORSISTEMA,ESTADO,HORA,OBSERVACION,RELOJ FROM fichada WHERE USERINFOID = ? AND ESTADO = 1 ORDER BY FECHA,HORA ASC";
    public static final String fichadaMes2 = "SELECT USERINFOID,FECHA,FICHADAPORSISTEMA,ESTADO,HORA,OBSERVACION,RELOJ FROM fichada WHERE USERINFOID = ? AND FECHA >= ? AND FECHA <= ? AND ESTADO = 1 ORDER BY FECHA,HORA ASC";
    public static final String fichadaMes3 = "SELECT USERINFOID,FECHA,FICHADAPORSISTEMA,ESTADO,HORA,OBSERVACION,RELOJ FROM fichada WHERE USERINFOID = ? AND FECHA = ? AND ESTADO = 1 ORDER BY FECHA,HORA ASC";
    public static final String grupoTodos = "SELECT * FROM grupo";
    public static final String consultaPersonaGrupoALL = "SELECT ID, USERINFOID, GRUPO_ID FROM personagrupo";
    public static final String personaGrupoPorId = "SELECT ID, USERINFOID,GRUPO_ID FROM personagrupo WHERE USERINFOID = ?";
    public static final String grupoPorID = "SELECT * FROM grupo WHERE ID = ?";
    public static final String licenciaTodo = "SELECT ID, DESCUENTO, DETALLE FROM licencia ORDER BY DETALLE ASC";

    public static final String empleadoLicenciaTodoID = "SELECT * FROM licenciaempleado WHERE USERINFOID = ? AND JUSTIFICACIONFALTA = 0 AND ACCION <> 'B'";
    public static final String consultaEnLicenia = "SELECT * FROM licenciaempleado WHERE USERINFOID = ? AND (FECHA_INICIO <= ? AND FECHA_FIN > ?) AND ACCION <> 'B'";
    public static final String consultaLicenciasDeMes = "SELECT * FROM licenciaempleado WHERE USERINFOID = ? AND (FECHA_INICIO >= ? AND FECHA_INICIO <= ?) AND ACCION <> 'B';";

    public static final String consultaFichadaXsistema = "SELECT USERINFOID,FECHA,FICHADAPORSISTEMA,ESTADO,HORA,OBSERVACION,RELOJ FROM fichada WHERE FECHA = ? AND USERINFOID = ? AND HORA = ? AND ESTADO = 1";
    public static final String consultaExisteFichada = "SELECT USERINFOID,FECHA,FICHADAPORSISTEMA,ESTADO,HORA,OBSERVACION,RELOJ FROM fichada WHERE USERINFOID = ? AND FECHA = ? AND HORA = ? AND ESTADO = 1";
    public static final String consultaFeriados = "SELECT ID, FECHA, DETALLE FROM feriado WHERE FECHA LIKE ? ORDER BY FECHA ASC;";
    public static final String consultaFeriadosMes = "SELECT ID, FECHA, DETALLE FROM feriado WHERE FECHA LIKE ? AND FECHA LIKE ? ORDER BY FECHA ASC;";
    public static final String consultaFeriadosTodos = "SELECT ID, FECHA, DETALLE FROM feriado ORDER BY FECHA ASC;";
    public static final String consultaFeriado = "SELECT ID, FECHA, DETALLE FROM feriado WHERE FECHA = ? ORDER BY FECHA ASC;";
    public static final String consultaFuncionLegajo = "SELECT FuncLega FROM personal.legajo WHERE CucuPers = ?";
    public static final String consultaFuncionProveedores = "SELECT DetaProv FROM contpres.proveedor WHERE CucuPers = ?";
    public static final String consultExisteLicenciaEmp = "SELECT * FROM licenciaempleado WHERE USERINFOID = ? AND LICENCIA_ID = ? AND DETALLE = ? AND ACCION <> 'B'";
    public static final String nombreEmpleadoPersonal = "SELECT infogov.persona.cucupers, infogov.persona.detapers, personal.legajo.FuncLega FROM infogov.persona INNER JOIN personal.legajo ON personal.legajo.CucuPers = infogov.persona.CucuPers WHERE infogov.persona.CucuPers = ANY ( SELECT DISTINCT reloj_muni.fichada.CUCUPERS FROM reloj_muni.fichada)";
    public static final String nombreEmpleadoContpres = "SELECT infogov.persona.cucupers, infogov.persona.detapers, contpres.proveedor.DetaProv FROM infogov.persona INNER JOIN contpres.proveedor ON contpres.proveedor.CucuPers = infogov.persona.CucuPers WHERE infogov.persona.CucuPers = ANY ( SELECT DISTINCT reloj_muni.fichada.CUCUPERS FROM reloj_muni.fichada)";// AND contpres.proveedor.LocaProv = 1 //con esto traemos todos los que posean un contrato de locacion, hay casos donde esto es cero pero igual trabajan por eso es dejado de usar
    public static final String consultaUserinfoALL = "SELECT * FROM USERINFO WHERE userid > 6000000;";
    public static final String consultaUserinfoFIND = "SELECT * FROM USERINFO WHERE ID = ?";
    public static final String consultaUserinfoFINDxUSERID = "SELECT * FROM USERINFO WHERE USERID = ? AND RELOJ = ?";
    public static final String consultaUserinfoID = "SELECT * FROM USERINFO WHERE ID = ?";
    public static final String consultaUserinfoUSERID = "SELECT * FROM USERINFO WHERE USERID = ?";
    public static final String consultaUserinfoCAPATAZ = "SELECT * FROM usuario WHERE TIPO = 'capataz' AND BAJA IS FALSE;";
    public static final String consultaUserinfoXDETALLE = "SELECT * FROM usuario WHERE USUARIO = ? AND ID_AREA <> 0 AND BAJA IS FALSE;";

    public static final String usuarioInfogovPorNombre = "SELECT * FROM usuario WHERE CodiUsua = ?";
    public static final String usuarioInfogovValidar = "SELECT infogov.fnValidarUsuario(?) AS Error";
    public static final String usuarioInfogovValidarClave = "select infogov.fnValidarClaveUsuario(?,?) as Error";
    public static final String usuarioOficinaPorUsuario = "SELECT * FROM USUARIO_OFICINA WHERE ID_USUARIO = ?";

    public static final String consultaUsuariosDeSistema = "SELECT * FROM usuario WHERE BAJA IS FALSE ORDER BY USUARIO ASC;";
    public static final String consultaUsuariosDeSistemaPorUsuario = "SELECT * FROM usuario WHERE USUARIO = ? AND BAJA IS FALSE;";
    public static final String consultaUsuariosDeSistemaPorId = "SELECT * FROM usuario WHERE ID = ? AND BAJA IS FALSE;";
    public static final String consultaUserinfoSinCapataz = "Select * from userinfo where CAPATAZ = 0 ORDER BY NAME asc";
//    public static final String consultaUserinfoSinCapatazPorNombre="Select * from userinfo where CAPATAZ = 0 and NAME Like ? ORDER BY NAME asc";
    public static final String consultaUserinfoSinCapatazPorSeccion = "Select * from userinfo where ID_SECCION = ? ORDER BY NAME asc";
    public static final String consultaUserinfoSinCapatazPorArea = "SELECT * FROM userinfo a \n"
            + "INNER JOIN seccion b ON b.ID = a.ID_SECCION\n"
            + "WHERE b.ID_AREA = ? ORDER BY NAME ASC;";
    public static final String consultaUserinfoSinCapatazPorUSUARIO = "SELECT\n"
            + "  a.* "
            + "FROM `reloj_muni3`.`userinfo` a\n"
            + "INNER JOIN reloj_muni3.seccion b ON b.ID = a.ID_SECCION\n"
            + "INNER JOIN reloj_muni3.area c ON c.ID = b.ID_AREA\n"
            + "INNER JOIN reloj_muni3.usuario d ON d.ID_AREA = c.ID\n"
            + "WHERE d.USUARIO = ? ORDER BY a.NAME ASC;";

    public static final String consultaAreas = "SELECT * FROM area WHERE BAJA = 0 ORDER BY detalle ASC";
    public static final String consultaAreaID = "SELECT * FROM area WHERE  BAJA = 0 AND ID = ?";
    public static final String consultaAreaDETALLE = "SELECT * FROM area WHERE  BAJA = 0 AND DETALLE LIKE ?  ORDER BY detalle ASC";

    public static final String consultaSecciones = "SELECT * FROM seccion WHERE BAJA = 0 ORDER BY detalle ASC";
    public static final String consultaSeccionesID = "SELECT * FROM seccion WHERE  BAJA = 0 AND ID = ?";
    public static final String consultaSeccionesDETALLE = "SELECT * FROM seccion WHERE  BAJA = 0 AND DETALLE LIKE ? ORDER BY detalle ASC";
    public static final String consultaSeccionesAREAS = "SELECT * FROM seccion WHERE BAJA = 0 AND ID_AREA = ? ORDER BY detalle ASC";

    public static final String consultaRelojes = "SELECT * FROM reloj WHERE ESTADO <> 0 ORDER BY reloj ASC;";
    public static final String consultaRelojesPorID = "SELECT * FROM reloj WHERE ID = ? AND ESTADO <> 0;";
    public static final String consultaRelojesPorNOMBRE = "SELECT * FROM reloj WHERE RELOJ = ? AND ESTADO <> 0;";

    public static final String consultaPathReloj = "SELECT PATHRELOJ FROM parametros WHERE ID = 1";
    public static final String consultaLoggs = "SELECT * FROM logger";
    public static final String consultaLoggsID = "SELECT * FROM logger WHERE ID = ?";

    //INSERTS//
    public static final String fichadaInsert = "INSERT INTO fichada (USERINFOID,FECHA,HORA,TERMINAL,IP,USUARIOSISTEMA,FECHASUBIDO) VALUES (?,?,?,?,?,?,CURRENT_TIMESTAMP())";
    public static final String grupoInster = "INSERT INTO grupo (NOMBRE,ENTRADA,SALIDA,HORADEALMUERZO,JORNADAEXTENDIDA) VALUES (?,?,?,?,?)";
    public static final String personaGrupoInsert = "INSERT INTO personagrupo (USERINFOID, GRUPO_ID) VALUES (?,?)";
    public static final String licenciaEmpleadoInsert = "INSERT INTO licenciaempleado (FECHA_FIN, FECHA_INICIO, USERINFOID, LICENCIA_ID,DETALLE,TERMINAL, IP, USUARIOSISTEMA, FECHAAUDITORIA, ACCION) VALUES (?,?,?,?,?,?,?,?,?,'A')";
    public static final String licenciaInsert = "INSERT INTO licencia (DESCUENTO, DETALLE) VALUES (?,?)";
    public static final String fichadaPorSistemaINSERT = "INSERT INTO fichada (USERINFOID,FECHA,FICHADAPORSISTEMA,HORA,OBSERVACION,TERMINAL,IP,USUARIOSISTEMA,FECHASUBIDO) VALUES (?,?,?,?,?,?,?,?,?)";
    public static final String fichadaDeAuditoriaINSERT = "SELECT fichadaDeAuditoria(?,?,?,?,?,?,?,?,?,?,?);";

    public static final String feriadoInsert = "INSERT INTO feriado (FECHA, DETALLE) VALUES (?,?)";
    public static final String userinfoInsert = "INSERT INTO USERINFO (USERID,NAME,CUCUPERS,TERMINAL,IP,USUARIOSISTEMA,FECHAAUDITORIA,ACCION) VALUES (?,?,?,?,?,?,CURRENT_TIMESTAMP(),'A');";

    public static final String usuarioDeSistemaINSERT = "INSERT INTO usuario (USUARIO,FECHAMODIFICACION,USERALTA,TIPO,ID_AREA, ID_SECCION) VALUES (?,?,?,?,?,?);";

    public static final String areaINSERT = "INSERT INTO area (DETALLE) VALUES (?)";

    public static final String seccionINSERT = "INSERT INTO seccion ( DETALLE, ID_AREA) VALUES ( ?, ?);";

    public static final String relojnINSERT = "INSERT INTO reloj (ID,RELOJ,IP) VALUES ( ?, ?, ? );";

    public static final String loggsINSERT = "INSERT INTO logger (FECHA, LUGARFALLA, NOMBREDELTERMINAL, DETALLES, NROERROR) VALUES (?, ?, ?, ?, ?);";

    //UPDATES//
    public static final String usuarioInfogovInhabilitar = "UPDATE usuario SET InhaUsua = 1, ObseUsua = ? WHERE CodiUsua = ?";
    public static final String updatePersonaGrupo1 = "UPDATE personagrupo SET USERINFOID = ?, GRUPO_ID = ? WHERE ID = ?";
    public static final String updateEmpleadoLic = "UPDATE licenciaempleado SET FECHA_FIN = ?, TERMINAL = ?, IP = ?, USUARIOSISTEMA = ?, FECHAAUDITORIA = ?, ACCION = 'M' WHERE ID = ?";
    public static final String deleteEmpleadoLic = "UPDATE licenciaempleado SET TERMINAL = ?, IP = ?, USUARIOSISTEMA = ?, FECHAAUDITORIA = ?, ACCION = 'B' WHERE ID = ?";
    public static final String updateLicencia = "UPDATE licencia SET DETALLE = ? WHERE ID = ?";
    public static final String fichadaPorSistemaUPDATE = "UPDATE fichada SET ESTADO = ? WHERE USERINFOID = ? AND FECHA = ? AND HORA = ?";
    public static final String fichadaDetalleUPDATE = "UPDATE fichada SET OBSERVACION = ? WHERE USERINFOID = ? AND FECHA = ? AND HORA = ?"; //MODIFICAR
    public static final String userinfoUPDATE = "UPDATE USERINFO SET USERID = ?, NAME = ?, CUCUPERS = ?, RELOJ = ?, ID_SECCION = ?, TERMINAL = ?, IP = ?, USUARIOSISTEMA = ?, FECHAAUDITORIA = ?, ACCION = 'M'  WHERE ID = ?";
    public static final String areaUPDATE = "UPDATE area SET DETALLE = ? WHERE ID = ?";
    public static final String seccionUPDATE = "UPDATE seccion SET DETALLE = ?, ID_AREA = ? WHERE ID = ?";
    public static final String usuariosDeSistemaUPDATE = "UPDATE usuario SET USUARIO = ?,FECHAMODIFICACION = ?,USERALTA = ?,TIPO = ?,ID_AREA = ?, ID_SECCION = ? WHERE ID = ?";
    public static final String relojUPDATE = "UPDATE reloj SET ID = ?, RELOJ = ?, IP = ?, ESTADO = ? WHERE ID = ?";
    public static final String grupoUpdate = "UPDATE grupo SET NOMBRE = ?,ENTRADA = ?,SALIDA = ?,HORADEALMUERZO = ?,JORNADAEXTENDIDA = ? WHERE ID = ?";

//DELETS//
    public static final String usuariosDeSistemaDELETE = "UPDATE usuario SET BAJA = ? WHERE ID = ?";
}
