/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.fichadas.ReporteExcel;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Loggs;

/**
 *
 * @author Viktor
 */
public class ContLogger extends ControladorGeneral {

    public void habrirLog() {

    }
    
// <editor-fold defaultstate="collapsed" desc="Manipulacion de txt"> 

    public void crearLogTXT() throws IOException {
        File file = new File("C:\\logs-reloj.txt");
        if (file.exists()) {
            borrarLogTXT(file);
            file.createNewFile();
//            System.out.println("SE A ELININADO Y CREADO EL  ARCHIVO DE NUEVO");
        } else {
//            System.out.println("SOLO SE CREO EL FILE");
            file.createNewFile();
        }
    }

    public void borrarLogTXT(File file) {
        file.delete();
    }

    public String leerLogTXT() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String lineaAcu = "";

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File("C:\\logs-reloj.txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            int i = 0;
            while ((linea = br.readLine()) != null) {
                if (i == 0) {
                    lineaAcu = linea;
                } else {
                    lineaAcu = lineaAcu + "\n" + linea;
                }
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        return lineaAcu;
    }

    public void escribirLogTXT(String texto) throws IOException {
        File file = new File("C:\\logs-reloj.txt");
        BufferedWriter bw;
        if (file.exists()) {
            bw = new BufferedWriter(new FileWriter(file));
            bw.write(texto);
        } else {
            bw = new BufferedWriter(new FileWriter(file));
            bw.write(texto);
        }
        bw.close();
    }
    // </editor-fold>
    
    public int insert(Loggs l){
        Object[] datos = {l.getFecha(), l.getLugarFalla(), l.getNombreDelTerminal(), l.getDetalles(), l.getNroError()};
        int id = ejecutarSentencia(datos, loggsINSERT, conexionRelojLocal);
        return  id;
    }
}
