/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.fichadas;

import controlador.Conectate;
import controlador.ContReloj;
import controlador.ContUserinfo;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
//import jdk.nashorn.internal.objects.NativeString;
import modelo.Fichada;
import modelo.Reloj;
import modelo.Userinfo;
import vista.V_UsuarioInicio;
import vista.V_principal;

/**
 *
 * @author Viktor
 */
public class FichadaReloj2 {

    Conectate conexion;
    String dir;

    public FichadaReloj2(String dir) {
        this.dir = dir;
    }

    public void conectar() {
        conexion = new Conectate(dir);
    }

    public void desconectar() {
        conexion.desconectar(dir);
    }

    public List<String> trerFichadass(String fecha, String codigo) {
        List<String> lista = new ArrayList<>();

        try {
            conectar();

            Connection conn = conexion.getConnection();

            PreparedStatement psmt = conn.prepareStatement("select  a.CHECKTIME from CHECKINOUT a \n"
                    + " where a.USERID = (select USERID from USERINFO where BADGENUMBER = ?  )"
                    + " and  Format([a.CHECKTIME], 'yyyy-mm-dd' ) = ? ");
            psmt.setString(1, codigo);
            psmt.setString(2, fecha);
            ResultSet rs = psmt.executeQuery();

//            rs.beforeFirst();
//            int i = 0;
            while (rs.next()) {
                lista.add(rs.getString(1).split(" ")[1]);
            }
//            System.out.println("Tamaño: " + i);

        } catch (Exception e) {
            System.out.println("problemas al buscar Contacto " + e.getMessage());
            e.printStackTrace();
        } finally {
            desconectar();
        }

        return lista;
    }

    private int relojid(String reloj) {

        int id = 0;

        switch (reloj) {
            case "Costa":
                id = 2;
                break;

            case "Cultura":
                id = 3;
                break;

            case "Maestranza":
                id = 1;
                break;

            case "Planta":
                id = 4;//ningun reloj de zksoftware tiene el id cero 0
                break;

            case "Frigorifico":
                id = 6;//ningun reloj de zksoftware tiene el id cero 0
                break;

            case "3 de Mayo":
                id = 5;//ningun reloj de zksoftware tiene el id cero 0
                break;
        }
        return id;
    }

    public String traerUlitimaMarcadaa(int reloj) {
        String ultimaMarcada = "";

        try {
            conectar();

            Connection conn = conexion.getConnection();

            PreparedStatement psmt = conn.prepareStatement("SELECT Max(CHECKINOUT.CHECKTIME) AS MáxDeCHECKTIME FROM CHECKINOUT WHERE (((CHECKINOUT.SENSORID)=?) AND ((CHECKINOUT.subidoBD)=True));");
            psmt.setString(1, reloj + "");
            ResultSet rs = psmt.executeQuery();

//            rs.beforeFirst();
//            int i = 0;
            while (rs.next()) {
                ultimaMarcada = rs.getString(1);
            }
//            System.out.println("Tamaño: " + i);

        } catch (Exception e) {
            System.out.println("problemas al buscar la ultima fichada registrada " + e.getMessage());
            e.printStackTrace();
        } finally {
            desconectar();
        }

        return ultimaMarcada;
    }

    public List<Fichada> trerFichadasTodos(String usuarioSistema) {

        List<Fichada> lista = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, -30);
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);

        System.out.println("año: " + (anio));
        System.out.println("mes: " + (mes));
        System.out.println("dia: " + (dia));

        System.out.println("mes anterior: " + (mes));
        
            // MEJORAR CONEXION
//            String fecha1 = traerUlitimaMarcada(sensorid);
//            String fecha2 = getFechaHOY();
        ContUserinfo contUserinfo = new ContUserinfo();
        List<Userinfo> listaUsuarios = contUserinfo.listar();
        String time = "";
        try {
            conectar();

            Connection conn = conexion.getConnection();

            Statement smt = conn.createStatement();


            System.out.println("SELECT A.BADGENUMBER, B.CHECKTIME, B.USERID, B.SENSORID  FROM CHECKINOUT B"
                    + " INNER JOIN USERINFO A ON A.USERID = B.USERID " 
                    + " WHERE subidoBD = false AND B.CHECKTIME >= #" + (anio) + "-" + (mes < 10 ? "0"+mes : mes) + "-"+(dia < 10 ? "0"+dia : dia)+" 00:00:00# AND CLng(A.BADGENUMBER) > 1000"
                    + " ORDER BY A.BADGENUMBER, B.CHECKTIME");
            
            ResultSet rs = smt.executeQuery("SELECT A.BADGENUMBER, B.CHECKTIME, B.USERID, B.SENSORID  FROM CHECKINOUT B"
                    + " INNER JOIN USERINFO A ON A.USERID = B.USERID "
                    + " WHERE subidoBD = false AND B.CHECKTIME >= #" + (anio) + "-" + (mes < 10 ? "0"+mes : mes) + "-"+(dia < 10 ? "0"+dia : dia)+" 00:00:00# AND CLng(A.BADGENUMBER) > 1000"
                    + " ORDER BY A.BADGENUMBER, B.CHECKTIME");
//                System.out.println("tamaño: " + rs.getFetchSize
            System.out.println("rs: "+rs.getFetchSize());
            while (rs.next()) {
                Long userinfoid = rs.getLong("BADGENUMBER");
                Userinfo u = contUserinfo.encontrar(listaUsuarios, userinfoid);
                if (u != null) {//no encontro el usuario asi q no carga la fichada

                    Fichada fichada = new Fichada();

                    fichada.setUserinfoid(u.getId());
                    time = rs.getString("CHECKTIME");
//                Timestamp t = Timestamp.valueOf(rs.getString("CHECKTIME"));
                    fichada.setFecha(Date.valueOf(time.split(" ")[0]));
                    fichada.setHora(Time.valueOf(time.split(" ")[1].substring(0, 8)));//(new Time(t.getTime()));


                    fichada.setTerminal(contUserinfo.getNombreTerminal());
                    fichada.setIp(contUserinfo.getIpTerminal());
                    fichada.setUsuario(usuarioSistema);

//                System.out.println(fichada.getFechaHora()+" "+ fichada.getUserid());
                    lista.add(fichada);
//                        checarSubidoBD(userid,time);
                } else {
                    //System.err.println("Un usuario sin dar de alta documento: "+userinfoid);
                }

            }

        } catch (Exception e) {
            System.out.println("problemasSS: " + e.getMessage());

            e.printStackTrace();
        } finally {
            desconectar();
        }

        return lista;
    }

//    public List<Fichada> traerFichadaMes(int anio, int mes, Long empleado) throws IOException {
//        Utilidades u = new Utilidades();
//        List<String> listaFechas = u.fechas(anio, mes);
//        List<Fichada> listaFichada = new ArrayList<Fichada>();
//
//        for (String fecha : listaFechas) {
//            for (String es : trerFichadas(fecha, "" + empleado)) {
//                Fichada f = new Fichada();
//                f.setCucuPers(empleado);
//                f.setFecha(Date.valueOf(fecha));
//                f.setHora(Time.valueOf(es));//(new Time(Timestamp.valueOf(fecha+" "+es).getTime()));
//                listaFichada.add(f);
//            }
//        }
//
//        return listaFichada;
//    }
    public List<Userinfo> traerEmpleados() {
        List<Userinfo> listaEmpleados = new ArrayList<>();
        conectar();
        try {

            Connection conn = conexion.getConnection();

            PreparedStatement psmt = conn.prepareStatement("SELECT BADGENUMBER, NAME FROM USERINFO WHERE BADGENUMBER > 6000000 ORDER BY USERID ASC");

            ResultSet rs = psmt.executeQuery();

            while (rs.next()) {
                
                Userinfo e = new Userinfo(rs.getLong(1), rs.getString(2), 0);
                listaEmpleados.add(e);
            }

        } catch (Exception e) {
            System.out.println("problemas error 0001: " + e.getMessage());
            e.printStackTrace();
        } finally {
            desconectar();
        }
        return listaEmpleados;
    }

    public String getFechaHOY() {
        Timestamp hoy = new Timestamp(Calendar.getInstance().getTimeInMillis());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return df.format(hoy);
    }
}
