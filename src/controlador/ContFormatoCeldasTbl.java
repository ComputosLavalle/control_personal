/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.Component;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import modelo.Feriado;
import modelo.Fichada;

/**
 *
 * @author Viktor
 */
public class ContFormatoCeldasTbl extends DefaultTableCellRenderer {

    List<List<Fichada>> listaLFichadas;
    List<Feriado> listaFeriados;

    public void setListaLFichadas(List<List<Fichada>> l) {
        this.listaLFichadas = l;
    }

    public void setListaFeriados(List<Feriado> listaFeriadoMes) {
        listaFeriados = listaFeriadoMes;
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected,
        
            boolean focused, int row, int column) {
        //Si values es nulo dara problemas de renderizado, por lo tanto se pone como vacio
        
        JLabel lbl = new JLabel(value == null ? "" : value.toString());
        if (column == 0 || column >= 7) {
            lbl.setOpaque(true);
            lbl.setBackground(Color.decode("#BDBDBD"));
            lbl.setForeground(Color.BLUE);
        }
        if (column > 0 && column < 7) {
            lbl.setOpaque(true);
            if (!lbl.getText().equals("")) {
                Timestamp fechahora = Timestamp.valueOf(table.getValueAt(row, 0).toString() + " " + value.toString());
                
                if (column > 0 && column < 7) {
//                    System.err.println("asdfjaskjklfa "+listaLFichadas.get(row).get(column-1).toString());
                    if (listaLFichadas.get(row).get(column-1).isFichadaPorSistema()) {
                        lbl.setFont(new java.awt.Font("Tahoma", 1, 11));//fuente tahoma, negrita, tamaño 11
                        lbl.setToolTipText(listaLFichadas.get(row).get(column-1).getObservacion());
                    }else if(!"".equals(listaLFichadas.get(row).get(column-1).getObservacion())){
                        lbl.setToolTipText(listaLFichadas.get(row).get(column-1).getObservacion());
                    }
                }

            }
        }

        if (table.getSelectedRow() != -1 && row == table.getSelectedRow()) {
            lbl.setBackground(Color.decode("#8181F7"));
        }

        if (row >= 0) {
            try{
            lbl.setOpaque(true);
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(Timestamp.valueOf(table.getValueAt(row, 0).toString() + " 00:00:00").getTime());
            if (c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7) {
                lbl.setBackground(Color.decode("#A4A4A4"));
            }
            }catch(Exception e){
                System.out.println("Error en contFormatoCeldasTbl: "+e.getMessage());
            }

        }

        for (Feriado f : listaFeriados) {
            if (table.getValueAt(row, 0).toString().equals(f.getFecha().toString())) {
                lbl.setBackground(Color.decode("#F5A9A9"));
                lbl.setToolTipText(f.getFecha()+ " "+ f.getDetalle());
            }
        }

        if (table.getSelectedRow() != -1 && row == table.getSelectedRow()) {
            lbl.setBackground(Color.decode("#8181F7"));
        }

        return lbl;
    }
}
