
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.optimizando;

import controlador.Conectate;
import controlador.ControladorGeneral;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.optimizando.Usuario_OficinaOpt;

/**
 *
 * @author fernando
 */
public class ContUsuario_OficinaOpt extends ControladorGeneral{
    
    
//    SISTEMA LOCAL
//    private String url = "jdbc:mysql://192.168.1.149:3306/central";
//    private String login = "pasante";
//    private String password = "qwerty";
//    private String bd = "central";
////    
    //IP MUNI

//    private String url = "jdbc:mysql://localhost:3307/Central";
    private String url = "jdbc:mysql://"+conexionCentral[3]+":3307/central";//"jdbc:mysql://localhost:3307/infogov";
    private String login = conexionCentral[1];;
    private String password = conexionCentral[2];
    private String bd = conexionCentral[0];
    
    public List<Usuario_OficinaOpt> listarTodos() {
        List<Usuario_OficinaOpt> listaUsuario_oficina = new ArrayList<>();

         Conectate conexion = null;
        
        try {

            
            conexion = new Conectate(login, password, url);
            Connection conn = conexion.getConnection(); 
            PreparedStatement pe = conn.prepareStatement("SELECT * FROM USUARIO_OFICINA");

            ResultSet rs = pe.executeQuery();

            while (rs.next()) {
                listaUsuario_oficina = setearEnLista(rs, listaUsuario_oficina);
            }


        } catch (Exception e) {
            System.out.println("Problemas al listare los usuarios oficinas - Controlador oPTIMIZADO" + e.getMessage() + e.getStackTrace());
        } finally {

            if (conexion != null) {
                conexion.desconectar(bd);
            }

        }

        return listaUsuario_oficina;
    }

    public List<Usuario_OficinaOpt> buscarPorOficina(int oficina) {
        List<Usuario_OficinaOpt> listaUsuario_oficina = new ArrayList<>();

         Conectate conexion = null;
        
        try {

            
            conexion = new Conectate(login, password, url);
            Connection conn = conexion.getConnection(); 
            PreparedStatement pe = conn.prepareStatement("SELECT * FROM USUARIO_OFICINA where id_oficina = ?");
            
            pe.setInt(1, oficina);

            ResultSet rs = pe.executeQuery();

            while (rs.next()) {
                listaUsuario_oficina = setearEnLista(rs, listaUsuario_oficina);
            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar usuarios oficinas por oficina- Controlador" + e.getMessage() + e.getStackTrace());
        }finally {

            if (conexion != null) {
                conexion.desconectar(bd);
            }

        }

        return listaUsuario_oficina;
    }

    public List<Usuario_OficinaOpt> buscarPorUsuario(String usuario) {
        List<Usuario_OficinaOpt> listaUsuario_oficina = new ArrayList<>();

         Conectate conexion = null;
        
        try {

            
            conexion = new Conectate(login, password, url);
            Connection conn = conexion.getConnection(); 
            PreparedStatement pe = conn.prepareStatement("SELECT * FROM USUARIO_OFICINA where id_usuario = ?");
            
            pe.setString(1, usuario);

            ResultSet rs = pe.executeQuery();

            while (rs.next()) {
                listaUsuario_oficina = setearEnLista(rs, listaUsuario_oficina);
            }

                      

        } catch (Exception e) {
            System.out.println("Problemas al buscar usuario oficina por nombreUsuario- Controlador" + e.getMessage() + e.getStackTrace());
        }finally {

            if (conexion != null) {
                conexion.desconectar(bd);
            }

        }

        return listaUsuario_oficina;
    }

//    public void setOficina(String usuario, int oficina) {
//        Usuario_oficina usuario_oficina = new Usuario_oficina();
//        usuario_oficina.setId_usuario(usuario);
//        usuario_oficina.setId_oficina(oficina);
//        em = getEntityManager();
//        try {
//            em.getTransaction().begin();
//
//
//
//            em.getTransaction();
//
//        } catch (Exception e) {
//            System.out.println("Problemas al buscar usuario oficina por nombreUsuario- Controlador" + e.getMessage() + e.getStackTrace());
//        }finally{
//            em.close();
//        }
//    }
    
    private List<Usuario_OficinaOpt> setearEnLista(ResultSet rs, List<Usuario_OficinaOpt> listaUsuario_oficina) throws SQLException{
        Usuario_OficinaOpt u = new Usuario_OficinaOpt();
                u.setId(rs.getInt("id"));
                u.setId_oficina(rs.getInt("id_oficina"));
                u.setId_usuario(rs.getString("id_usuario"));
                listaUsuario_oficina.add(u);
                return listaUsuario_oficina;
    }

    public Usuario_OficinaOpt setearEnObjeto(List list) {
        Usuario_OficinaOpt usuario_OficinaOpt = new Usuario_OficinaOpt(list.get(2).toString(), Integer.parseInt(list.get(1).toString()));
        return usuario_OficinaOpt;
    }
    
}
