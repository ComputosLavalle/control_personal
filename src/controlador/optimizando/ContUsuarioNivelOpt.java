/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.optimizando;

import controlador.Conectate;
import controlador.ControladorGeneral;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.optimizando.Usuario_NivelOpt;

/**
 *
 * @author fernando
 */
public class ContUsuarioNivelOpt extends ControladorGeneral{
    
//    SISTEMA LOCAL
//    private String url = "jdbc:mysql://localhost:3306/central?useServerPrepStmts=true";
//    private String login = "root";
//    private String password = "";
//    private String bd = "mesa";
////    
    //IP MUNI

    private String url = "jdbc:mysql://"+conexionCentral[3]+":3307/central";//"jdbc:mysql://localhost:3307/infogov";
    private String login = conexionCentral[1];;
    private String password = conexionCentral[2];
    private String bd = conexionCentral[0];
    
    public void crearUsuario_Nivel(Usuario_NivelOpt usuario_Nivel){
        Conectate conectate = null;
        
        try {
            conectate = new Conectate(login, password, url);
            Connection conn = conectate.getConnection();
            PreparedStatement pe = conn.prepareStatement("INSERT INTO USUARIO_NIVEL (ID_NIVEL,ID_USUARIO) VALUES (?,?)");
            pe.setInt(1, usuario_Nivel.getId_nivel());
            pe.setString(2, usuario_Nivel.getId_usuario());
            pe.executeQuery();
            
            
        } catch (Exception e) {
            System.out.println("problemas al crear UsuarioNivel "+e.getMessage());
        } finally {

            if (conectate != null) {
                conectate.desconectar(bd);
            }

        }
    }
    
    public List<Usuario_NivelOpt> buscarPorNombre(String nombre) {
        
        List<Usuario_NivelOpt> listaUsuario_nivel = new ArrayList<>();
        Conectate conexion = null;
        try {
            conexion = new Conectate(login, password, url);
            Connection conn = conexion.getConnection(); 
            PreparedStatement pe = conn.prepareStatement("SELECT * FROM USUARIO_NIVEL where ID_USUARIO = ?");

            pe.setString(1, nombre);


            ResultSet rs = pe.executeQuery();

            while (rs.next()) {
                Usuario_NivelOpt u = new Usuario_NivelOpt();
                u.setId_nivel(rs.getInt("ID_NIVEL"));
                listaUsuario_nivel.add(u);
            }
            
        } catch (Exception e) {
            System.out.println("Problemas al buscar Usuairo Nivel por Nombre optimizado");
            e.printStackTrace();
        } finally {

            if (conexion != null) {
                conexion.desconectar(bd);
            }

        }
        
        return listaUsuario_nivel;
        
    }

    public Usuario_NivelOpt setearEnObjeto(List list) {
      Usuario_NivelOpt usuario_NivelOpt = new Usuario_NivelOpt(list.get(2).toString(), Integer.parseInt(list.get(1).toString()));
      return usuario_NivelOpt;
    }
    
}
