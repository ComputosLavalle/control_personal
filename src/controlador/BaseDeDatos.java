/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author Ing. Gerardo Magni
 */
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 * Clase encargada de conexión y consultas a base de datos.
 *
 */
public class BaseDeDatos {

    /**
     * La conexion con la base de datos
     */
//    private Connection conexion = null;
//    private String user = "root";
    //private String user = "postgres";
//    private String password = "root";
    private String puerto = "3307";
    //private String puertoPosgres = "5432";
//    private String catalogo = "Central";
//    private String host = "localhost";
    /**
     * Se establece la conexion con la base de datos
     * @param conexion
     * @param datosConexion
     * @return 
     */
    public Connection estableceConexion(Connection conexion,String[] datosConexion) {
//        if (conexion != null)
//            return conexion;
        String base = datosConexion[0];
        String user = datosConexion[1];
        String password = datosConexion[2];
        String host = datosConexion[3];
        if(datosConexion.length == 5){
            puerto = datosConexion[4];
        }

        try {
            // Se registra el Driver de MySQL
            Class.forName("com.mysql.jdbc.Driver");

            // Se registra el Driver de PostGres
            //Class.forName("org.postgresql.Driver");
            //URL MySQL
            String urlConexion = "jdbc:mysql://" + host + ":" + puerto + "/" + base;
            //URL Postgres
            //String urlConexion = "jdbc:postgresql://" + host + ":" + puertoPosgres + "/"+catalogo;    

            // Se obtiene una conexión con la base de datos. Hay que
            // cambiar el usuario "root" y la clave "root" por las
            // adecuadas a la base de datos que estemos usando.
            conexion = DriverManager.getConnection(urlConexion, user, password);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: conexión a base de datos no establecida","Error",JOptionPane.ERROR_MESSAGE);
            e.getMessage();
        }
        return conexion;
    }

    /**
     * Cierra la conexión con la base de datos
     */
    public void cierraConexion(Connection conexion) {
        try {
            conexion.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
