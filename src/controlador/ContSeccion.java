/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.List;
import modelo.Seccion;

/**
 *
 * @author Viktor
 */
public class ContSeccion  extends ControladorGeneral{

    public ContSeccion() {
        super();
    }
    
    private Seccion setearObjeto(List l){
        return new Seccion(Integer.parseInt(l.get(0).toString()), l.get(1).toString(), Integer.parseInt(l.get(2).toString()));
    }
    
    public List<Seccion> encontrarTodos(){
        List<Seccion> listaReturn = new ArrayList<>();
        List<List> lista = dameListaCompleta(columnasSecciones, consultaSecciones, conexionRelojLocal);
        
        for (List lista1 : lista) {
            listaReturn.add(setearObjeto(lista1));
        }
        
        return listaReturn;
    }
    
    public Seccion encontrar(int id){
       Seccion seccion = null;
       Object[] datos = {id};
       List<List> lista = buscarPorParametro(columnasSecciones, consultaSeccionesID, datos, conexionRelojLocal);
       
        for (List lista1 : lista) {
            seccion = setearObjeto(lista1);
        }
       
       return seccion;
    }
    
    public int insert(Seccion s){
        Object[] datos =  {s.getDetalle(),s.getIdArea()};
        return  ejecutarSentencia(datos, seccionINSERT, conexionRelojLocal);
    }
    
    public int update(Seccion s){
        Object[] datos = {s.getDetalle(), s.getIdArea(), s.getId()};
        return ejecutarSentencia(datos, seccionUPDATE, conexionRelojLocal);
    }

    public List<Seccion> encontrarPorArea(int id) {
        List<Seccion> listaReturn = new ArrayList<>();
        Object[] datos = {id};
        List<List> lista = buscarPorParametro(columnasSecciones, consultaSeccionesAREAS, datos , conexionRelojLocal);
        
        for (List l : lista) {
            listaReturn.add(setearObjeto(l));
        }
        
        return  listaReturn;
    }
    
}
