/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import modelo.Grupo;
import modelo.PersonaGrupo;

/**
 *
 * @author Viktor
 */
public class ContGrupo extends ControladorGeneral {
    
    public ContGrupo() {
        super();
    }
    
    Grupo setearEnObjeto(List lista) {
        
        Grupo grupo
                = new Grupo(Integer.parseInt(lista.get(0).toString()),
                        lista.get(1).toString(),
                        new Time(((Time) lista.get(2)).getTime()),
                        new Time(((Time) lista.get(3)).getTime()),
                        Boolean.parseBoolean(lista.get(4).toString()),
                        Boolean.parseBoolean(lista.get(5).toString()));
        
        return grupo;
    }
    
    public void insert(Grupo grupo) {
        
        Object[] arreglo = {grupo.getNombre(), grupo.getEntrada(), grupo.getSalida(),grupo.isHoraDeAlmuerzo(), grupo.isJornadaExtendida()};
        
        ejecutarSentencia(arreglo, BDSentencias.grupoInster, BDSentencias.conexionRelojLocal);
    }
    
    public void update(Grupo grupo) {
        
        Object[] arreglo = {grupo.getNombre(), grupo.getEntrada(), grupo.getSalida(),grupo.isHoraDeAlmuerzo(), grupo.isJornadaExtendida(), grupo.getId()};
        
        ejecutarSentencia(arreglo, BDSentencias.grupoUpdate, BDSentencias.conexionRelojLocal);
    }
    
    public List<Grupo> findAll() {
        
        List<Grupo> lista = new ArrayList<>();
        
        int i = 0;
        List<List> list = dameListaCompleta(BDSentencias.grupoColumnas, BDSentencias.grupoTodos, BDSentencias.conexionRelojLocal);
        Grupo g = null;
        for (List l : list) {
            lista.add(setearEnObjeto(l));
        }
        
        return lista;
    }
    
    public Grupo buscarPorCuil(int id) {
        Object[] datos = {id};
        int id_grupo = 0;
        List<List> lista = buscarPorParametro(BDSentencias.personaGrupoColumnas, BDSentencias.personaGrupoPorId, datos, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            id_grupo = (int) l.get(2);
        }
        
        Grupo g = null;
        Object[] datos2 = {id_grupo};
        List<List> lista2 = buscarPorParametro(BDSentencias.grupoColumnas, BDSentencias.grupoPorID, datos2, BDSentencias.conexionRelojLocal);
        for (List l : lista2) {
            g = new Grupo();
            g.setId((int) l.get(0));
            g.setNombre(l.get(1).toString());
            g.setEntrada(Time.valueOf(l.get(2).toString()));
            g.setSalida(Time.valueOf(l.get(3).toString()));;
        }
        
        return g;
    }
    
    public int encontrarIDRelacion(Long cuil) {
        Object[] datos = {cuil};
        int p = 0;
        List<List> lista = buscarPorParametro(BDSentencias.personaGrupoColumnas, BDSentencias.personaGrupoPorId, datos, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            p = (int) l.get(0);
        }
        
        return p;
    }
    
}
