/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.util.ArrayList;
import java.util.List;
import modelo.Licencia;
import modelo.LicenciaEmpleado;

/**
 *
 * @author Viktor
 */
public class ContLicencia extends ControladorGeneral{

    public ContLicencia() {
    }

    public List<Licencia> findAll() {
        List<Licencia> lista = new ArrayList<>();
        
        List<List> ldl = dameListaCompleta(BDSentencias.licenciaColumnas, BDSentencias.licenciaTodo, conexionRelojLocal);
        for(List l: ldl){
            Licencia lic = new Licencia();
            lic.setId(Integer.parseInt(l.get(0).toString()));
            lic.setDescuento(Boolean.valueOf(l.get(1).toString()));
            lic.setDetalle(l.get(2).toString());
            lista.add(lic);
        }
        
        return lista;
    }

    public int create(Licencia l) {
        Object [] datos = {l.isDescuento(),l.getDetalle()};
        
        return ejecutarSentencia(datos, BDSentencias.licenciaInsert, BDSentencias.conexionRelojLocal);
        
    }

    public int update(Licencia l) {
        
        Object [] datos = {l.getDetalle(),l.getId()};
        
        return ejecutarSentencia(datos, BDSentencias.updateLicencia, BDSentencias.conexionRelojLocal);
    }

    public boolean existe(LicenciaEmpleado licenciaEmpleado) {
        boolean result = false;
        Object [] datos = {licenciaEmpleado.getUserinfoid(),licenciaEmpleado.getLicencia(), licenciaEmpleado.getDetalle()};
        
        List<List> lista = buscarPorParametro(BDSentencias.empleadolicecniaColumnas, BDSentencias.consultExisteLicenciaEmp, datos, BDSentencias.conexionRelojLocal);
        
        if(lista.size() > 0){
            result = true;
        }
        
        return result;
    }
    
}
