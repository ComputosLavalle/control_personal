/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.List;
import modelo.Reloj;

/**
 *
 * @author Viktor
 */
public class ContReloj extends ControladorGeneral{

    public ContReloj() {
        super();
    }
    
    private Reloj setearObjeto(List l){
        return new Reloj(Integer.parseInt(l.get(0).toString()), l.get(1).toString(), l.get(2).toString(), Boolean.parseBoolean(l.get(3).toString())); 
    }
    
    public List<Reloj> encontrarTodos(){
        List<Reloj> lista = new ArrayList<>();
        List<List> result = dameListaCompleta(columnasReloj, consultaRelojes, conexionRelojLocal);
        
        for (List l : result) {
            lista.add(setearObjeto(l));
        }
        
        return lista;
    }
    
    public Reloj encontrarPorId(int id){
        Object[] datos = {id};
        Reloj reloj = new Reloj();
        List<List> result = buscarPorParametro(columnasReloj, consultaRelojesPorID,datos, conexionRelojLocal);
        
        for (List l : result) {
            reloj = setearObjeto(l);
        }
        return reloj;
    }
    
    public Reloj encontrarPorNombre(String nombre){
        Object[] datos = {nombre};
        Reloj reloj = new Reloj();
        List<List> result = buscarPorParametro(columnasReloj, consultaRelojesPorNOMBRE,datos, conexionRelojLocal);
        
        for (List l : result) {
            reloj = setearObjeto(l);
        }
        return reloj;
    }
    
    public int insert(Reloj r){
        Object[] datos = {r.getId(), r.getNombre(), r.getIp()};
        int result = ejecutarSentencia(datos, relojnINSERT, conexionRelojLocal);
        return result;
    }
    
    public int update(Reloj r){
        Object[] datos = {r.getId(), r.getNombre(), r.getIp(),r.isEstado(),r.getId()};
        int result = ejecutarSentencia(datos, relojUPDATE, conexionRelojLocal);
        return result;
    }

    public String traerPath() {
        String retornar = "";
        
        List<List> list = dameListaCompleta(columnasPathReloj, consultaPathReloj, conexionRelojLocal);
        
        for (List l : list) {
            retornar = l.get(0).toString();
        }
        return retornar;
    }
    
    
}
