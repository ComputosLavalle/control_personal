/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author Aleatrix
 */
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.table.DefaultTableModel;
import modelo.Fichada;
/**
 * Convierte un ResultSet en un DefaultTableModel *
 */
public class GestorTabla {

    /**
     * Rellena el DefaultTableModel con los datos del ResultSet. Vacía el
     * DefaultTableModel completamente y le mete los datos que hay en el
     * ResultSet.
     *
     * @param rs El resultado de lac onsula a base de datos.
     * @param modelo El DefaultTableModel que queremos rellenar
     */
    public static void rellena(List rs, DefaultTableModel modelo, int caso) {
        //configuraColumnas(rs, modelo); //con este metodo configuro las columnas de forma automatica
        vaciaFilasModelo(modelo);
        if (caso < 100) {
            anhadeFilasDeDatos(rs, modelo, caso);
        } else if (caso > 100) {
            //anhadeFilasDeDatosTramiteCodigo(rs, modelo, caso);
        }
        //  System.out.println("entro al rellena");
    }

    /**
     * Añade al DefaultTableModel las filas correspondientes al ResultSet.
     *
     * @param rs El resultado de la consulta a base de datos
     * @param modelo El DefaultTableModel que queremos rellenar.
     */
    public static void anhadeFilasDeDatos(List rs, DefaultTableModel modelo, int caso) {
        //System.out.println("entro al aniade filas de datos");
        try {
            //System.out.println("try aniade fila de datos");
            for (int i = 0; i < rs.size(); i++) {

                Object[] datosFila = new Object[modelo.getColumnCount()];
                ponerDatosEnArreglo(caso, i, rs, datosFila);
                modelo.addRow(datosFila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Borra todas las filas del modelo.
     *
     * @param modelo El modelo para la tabla.
     */
    public static void vaciaFilasModelo(final DefaultTableModel modelo) {
        try {
            while (modelo.getRowCount() > 0) {
                modelo.removeRow(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Pone en el modelo para la tabla tantas columnas como tiene el resultado
     * de la consulta a base de datos.
     *
     * @param rs Resultado de consulta a base de datos.
     * @param modelo Modelo de la tabla.
     */
    private static void ponerDatosEnArreglo(int caso, int i, List rs, Object[] datosFila) {
        //System.out.println("entro a poner datos en arreglo");
        switch (caso) {

            case 1:
                
//                Hardware h = new Hardware();
//                h = (Hardware) rs.get(i);
//                datosFila[0] = h.getId();
                
                

                break;

            case 2:
                
                
                break;
        }


    }

    public static void configuraColumnas(int caso, DefaultTableModel modelo) {

        switch (caso) {

            case 1:
                modelo.addColumn("Fecha");
                modelo.addColumn("Entrada");
                modelo.addColumn("Salida");
                modelo.addColumn("Entrada");
                modelo.addColumn("Salida");
                modelo.addColumn("Entrada");
                modelo.addColumn("Salida");
                modelo.addColumn("Normales");
                modelo.addColumn("Extras");
                modelo.addColumn("Retraso");
                
                break;

            case 2:
                
                break;
        }

    }

    public static java.sql.Date parsearFecha(Date fecha) {
        java.sql.Date fechaSql = new java.sql.Date(fecha.getTime());

        return fechaSql;
    }

//    public static String getOficina(Long idOficina, List<Direcciones> listaDirecciones) {
//        String nombre = null;
//        for (Direcciones direccion : listaDirecciones) {
//            if (direccion.getId() == idOficina) {
//                nombre = direccion.getNombres();
//            }
//        }
//        return nombre;
//    }

//    public static void anhadeFilasDeDatosTramiteCodigo(List<TipoTramite> rs, DefaultTableModel modelo, int caso) {
//        //System.out.println("entro al aniade filas de datos");
//        try {
//            //System.out.println("try aniade fila de datos");
//            for (int i = 0; i < rs.size(); i++) {
//                for (int j = 0; j < rs.get(i).getListaCodigoPrecio().size(); j++) {
//                    Object[] datosFila = new Object[modelo.getColumnCount()];
//                    ponerDatosEnArreglo(caso, i, j, rs, datosFila);
//                    modelo.addRow(datosFila);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private static void ponerDatosEnArreglo(int caso, int i, int j, List rs, Object[] datosFila) {
//        //System.out.println("entro a poner datos en arreglo");
//        switch (caso) {
//
//
//            case 101:
//
//                TipoTramite tramiteCodigoPrecio = (TipoTramite) rs.get(i);
//
//                datosFila[0] = tramiteCodigoPrecio.getId();
//                datosFila[1] = tramiteCodigoPrecio.getNombre();
//                //try {
//                datosFila[2] = tramiteCodigoPrecio.getListaCodigoPrecio().get(j).getCodigo();
//                datosFila[3] = tramiteCodigoPrecio.getListaCodigoPrecio().get(j).getPrecio();
//                //} catch (ArrayIndexOutOfBoundsException e) {
//
//                //}
//
//                break;
//        }
//    }

    private static String formatearFecha(Timestamp fecha) {
        String formateada = "";

        try {
            formateada = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(fecha);

        } catch (Exception e) {
        }
        return formateada;
    }
}
