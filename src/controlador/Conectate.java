/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.swing.JOptionPane;


public class Conectate {

       Connection conn = null;
    
       /** Constructor de DbConnection */
       public Conectate(String accessFilePath){
          try{
             //obtenemos el driver de para acces
//             Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");//java 7
             //obtenemos la conexión
             Properties p = new Properties();
             p.put("charSet", "iso-8859-1");
              
              //DriverManager.getConnection("jdbc:ucanaccess://"+this.nombre_bd,this.usuarioBD,this.passwordBD);
             //conn = DriverManager.getConnection("jdbc:ucanaccess://"+accessFilePath,p);//java 7
             
             //conn = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Access Driver(*.mdb, *.accdb)};DBQ="+accessFilePath,p);//java 7
             Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");//java 8
             conn=DriverManager.getConnection("jdbc:ucanaccess://"+accessFilePath,p); //java 8
             if (conn!=null){
                 //System.out.println("Conexión exitosa!");
             }else{
                 JOptionPane.showMessageDialog(null,"Error al conectar a la based de datos","Error 001", JOptionPane.ERROR_MESSAGE);
             }
          }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e.getMessage(),"Error 002",JOptionPane.ERROR_MESSAGE);
          }
          catch(ClassNotFoundException e){//java 7
             JOptionPane.showMessageDialog(null,e.getMessage(),"Error 003",JOptionPane.ERROR_MESSAGE);
          }//java 7
       }
       
       public Conectate(String accessFilePath,String pass) {
          try{
             //obtenemos el driver de para acces
             Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
             //obtenemos la conexión
             
             Properties p = new Properties();
             p.put("charSet", "iso-8859-1");
             p.put("password", pass);
             conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=" +accessFilePath + ";PWD="+pass+";",p);
             if (conn!=null){
//                 System.out.println("Conexión exitosa!");
//                 System.err.println("------- -------");
             }else{
                 System.err.println("Error al conectar a la based de datos");
             }
          }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e.getMessage(),"Error 004",JOptionPane.ERROR_MESSAGE);
          }catch(ClassNotFoundException e){
             JOptionPane.showMessageDialog(null,e.getMessage(),"Error 005",JOptionPane.ERROR_MESSAGE);
          }
       }
       
       public Conectate(String login, String password, String url) {
          try{
             //obtenemos el driver de para mysql
             Class.forName("com.mysql.jdbc.Driver");
             //obtenemos la conexión
             Properties p = new Properties();
             p.put("charSet", "iso-8859-1");
             p.put("user", login);
             p.put("password", password);
             conn = DriverManager.getConnection(url,p);
             if (conn!=null){
    
             }
          }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e.getMessage(),"Error 006",JOptionPane.ERROR_MESSAGE);
          }catch(ClassNotFoundException e){
             JOptionPane.showMessageDialog(null,e.getMessage(),"Error 007",JOptionPane.ERROR_MESSAGE);
          }
       }
       
       /**Permite retornar la conexión*/
       public Connection getConnection(){
          return conn;
       }
    
       public void desconectar(String bd){
           try {
                conn.close();
//                System.out.println("Desconectado");
//                 System.err.println("--------------");
    
           } catch (Exception e) {
               JOptionPane.showMessageDialog(null,e.getMessage(),"Error 008",JOptionPane.ERROR_MESSAGE);
           }
    
       }
   
}