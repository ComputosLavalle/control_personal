/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author UPID
 */
public class ContUsuarioInfogov extends ControladorGeneral{
//    
//    SISTEMA LOCAL
    private String url = "jdbc:mysql://"+conexionInfogov[3]+":3307/infogov";//"jdbc:mysql://localhost:3307/infogov";
    private String login = conexionInfogov[1];;
    private String password = conexionInfogov[2];
    private String bd = conexionInfogov[0];
//    
    //sistema BD 
//    private String url = "jdbc:mysql://"+conexionInfogov[3]+":3306/infogov";//"jdbc:mysql://localhost:3307/infogov";
//    private String login = conexionInfogov[1];;
//    private String password = conexionInfogov[2];
//    private String bd = conexionInfogov[0];

    public ContUsuarioInfogov() {
    }

    public int buscarUsuarioInfoGovPorNombre(String nombre){
        
        Conectate conexion = null;

        int habilitado = 0;
       
        try {
            conexion = new Conectate("Usuario", "123456", url);

            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("SELECT * FROM usuario WHERE CodiUsua = '" + nombre+"'");

            while (rs.next()) {

                habilitado = rs.getInt("InhaUsua");
                
            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar el usuario por nombre -Controlador" + e.getMessage() + e.getStackTrace());
            e.printStackTrace();
        }finally{
            conexion.desconectar(bd);
        }        
        return habilitado;
    }
    
    public String fnValidarUsuario(String nombre){
        
        Conectate conexion = null;

        String usuario = null;
       
        try {
            conexion = new Conectate(login, password, url);

            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("SELECT infogov.fnValidarUsuario('"+nombre+"') AS Error");

            while (rs.next()) {

                usuario = rs.getString("Error");

            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar el usuario por nombre -Controlador" + e.getMessage() + e.getStackTrace());
        }finally{
            conexion.desconectar(bd);
        }        
        return usuario;
    }
    
    public String fnValidarClaveUsuario(String nombre, String contrasenia){
        
        Conectate conexion = null;

        String respuesta = null;
       
        try {
            conexion = new Conectate(login, password, url);

            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("select infogov.fnValidarClaveUsuario('"+nombre+"','"+contrasenia+"') as Error");

            while (rs.next()) {

                respuesta = rs.getString("Error");

            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar el usuario por nombre -Controlador" + e.getMessage() + e.getStackTrace());
        }finally{
            conexion.desconectar(bd);
        }        
        return respuesta;
    }

    public double prBuscarDeudaxCuil(String cuil) {
        Conectate conexion = null;

        double respuesta = 0;    
       
        try {
            conexion = new Conectate(login, password, url);

            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("CALL infogov.prBuscarDeudaxCuil('"+cuil+"')");

            while (rs.next()) {

                
                respuesta = rs.getDouble("deuda");
            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar el usuario por nombre -Controlador" + e.getMessage() + e.getStackTrace());
        }finally{
            conexion.desconectar(bd);
        }     
        System.out.println("deuda = " + respuesta);
        return respuesta;

    }

    public void inhabilitar(String usuario, String nombreTerminal, Timestamp fechaActual) {

    Conectate conexion = null;
    String observacion = null;
    
        try {
            conexion = new Conectate(login, password, url);
            
            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("SELECT ObseUsua FROM usuario WHERE CodiUsua = '"+usuario+"'");
            
            while (rs.next()) {

                
                observacion = rs.getString("ObseUsua");
            }
            
            Statement stm2 = conn.createStatement();

            stm2.executeUpdate("UPDATE usuario SET InhaUsua = 1, ObseUsua = '"+observacion+" Inhabilitado desde "+nombreTerminal +" "+fechaActual.toString()+"' WHERE CodiUsua = '"+usuario+"'");
            
        } catch (Exception e) {
            System.out.println("problemas: ");
            e.printStackTrace();
        }
    
    }
    
    public boolean verificarUsuarioYnivel(String nombre, int nivel){
        
        Conectate conexion = null;

        boolean habilitado = false;
       
        try {
            //conexion = new Conectate("Usuario", "123456", url);
            conexion = new Conectate("Usuario", "123456", url);

            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("SELECT IF((SELECT ID FROM USUARIO_NIVEL WHERE ID_USUARIO = '"+nombre+"' AND ID_NIVEL = "+nivel+") <> 0,TRUE,FALSE)");

            while (rs.next()) {

                habilitado = rs.getBoolean("IF((SELECT ID FROM USUARIO_NIVEL WHERE ID_USUARIO = '"+nombre+"' AND ID_NIVEL = "+nivel+") <> 0,TRUE,FALSE)");
                
            System.err.println("ENTROOOO!");
            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar el usuario por nombre -Controlador" + e.getMessage() + e.getStackTrace());
        }finally{
            conexion.desconectar(bd);
        }        
        return habilitado;
    }
    
    public List<String> buscarUsuarioInfoGov(String nombre){
        
        Conectate conexion = null;

        List<String> listaUsuarios = new ArrayList<>();
       
        try {
            conexion = new Conectate("Usuario", "123456", url);

            Connection conn = conexion.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery("SELECT CodiUsua FROM usuario WHERE CodiUsua LIKE '%" + nombre+"%'");

            while (rs.next()) {

                listaUsuarios.add(rs.getString("CodiUsua"));
                
            }

        } catch (Exception e) {
            System.out.println("Problemas al buscar el usuario por nombre -Controlador" + e.getMessage() + e.getStackTrace());
        }finally{
            conexion.desconectar(bd);
        }        
        return listaUsuarios;
    }
}
