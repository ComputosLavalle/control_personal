/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Timestamp;

/**
 *
 * @author Viktor
 */
public class Usuario {
    private int id;
    private String usuario;
    private Timestamp fechamodificacion;
    private String useralta;
    private String tipo;
    private int idaArea;
    private int idaSeccion;
    private boolean baja;

    public Usuario() {
    }

    public Usuario(int id, String usuario, Timestamp fechamodificacion, String useralta, String tipo, int idaArea, int idaSeccion, boolean baja) {
        this.id = id;
        this.usuario = usuario;
        this.fechamodificacion = fechamodificacion;
        this.useralta = useralta;
        this.tipo = tipo;
        this.idaArea = idaArea;
        this.idaSeccion = idaSeccion;
        this.baja = baja;
    }

    public Usuario(String usuario, Timestamp fechamodificacion, String useralta, String tipo, int idaArea, int idaSeccion) {
        this.usuario = usuario;
        this.fechamodificacion = fechamodificacion;
        this.useralta = useralta;
        this.tipo = tipo;
        this.idaArea = idaArea;
        this.idaSeccion = idaSeccion;
    }

    public boolean isBaja() {
        return baja;
    }

    public void setBaja(boolean baja) {
        this.baja = baja;
    }
    
    public int getIdaArea() {
        return idaArea;
    }

    public void setIdaArea(int idaArea) {
        this.idaArea = idaArea;
    }

    public int getIdaSeccion() {
        return idaSeccion;
    }

    public void setIdaSeccion(int idaSeccion) {
        this.idaSeccion = idaSeccion;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Timestamp getFechamodificacion() {
        return fechamodificacion;
    }

    public void setFechamodificacion(Timestamp fechamodificacion) {
        this.fechamodificacion = fechamodificacion;
    }

    public String getUseralta() {
        return useralta;
    }

    public void setUseralta(String useralta) {
        this.useralta = useralta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", usuario=" + usuario + ", fechamodificacion=" + fechamodificacion + ", useralta=" + useralta + ", tipo=" + tipo + ", idaArea=" + idaArea + ", idaSeccion=" + idaSeccion + '}';
    }
    
}
