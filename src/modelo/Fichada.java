/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Viktor
 */
@Entity
public class Fichada implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userinfoid;
    private Date fecha;
    private boolean fichadaPorSistema;
    private boolean estado;
    private Time hora;
    private String observacion;
    private String reloj;
    private String terminal;
    private String ip;
    private String usuario;
    private Timestamp fechasubido;

    public Fichada() {
    }

    public Fichada(int userinfoid, Date fecha,boolean fichadaPorSistema,boolean estado,Time hora,String observacion,String reloj) {
        this.userinfoid = userinfoid;
        this.fecha = fecha;
        this.fichadaPorSistema = fichadaPorSistema;
        this.estado = estado;
        this.hora = hora;
        this.observacion = observacion;
        this.reloj = reloj;
    }


    public String getReloj() {
        return reloj;
    }

    public void setReloj(String reloj) {
        this.reloj = reloj;
    }
    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }
    
    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    public boolean isFichadaPorSistema() {
        return fichadaPorSistema;
    }

    public void setFichadaPorSistema(boolean fichadaPorSistema) {
        this.fichadaPorSistema = fichadaPorSistema;
    }
    
    

    public int getUserinfoid() {
        return userinfoid;
    }

    public void setUserinfoid(int userinfoid) {
        this.userinfoid = userinfoid;
    }


    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Timestamp getFechasubido() {
        return fechasubido;
    }

    public void setFechasubido(Timestamp fechasubido) {
        this.fechasubido = fechasubido;
    }
    
    @Override
    public String toString() {
        return "Fichada{userinfoid=" + userinfoid + ", fecha=" + fecha + ", fichadaPorSistema=" + fichadaPorSistema + ", estado=" + estado + ", hora=" + hora + ", observacion=" + observacion + ", reloj=" + reloj + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + this.userinfoid;
        hash = 61 * hash + Objects.hashCode(this.fecha);
        hash = 61 * hash + Objects.hashCode(this.hora);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fichada other = (Fichada) obj;
        if (this.userinfoid != other.userinfoid) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (!Objects.equals(this.hora, other.hora)) {
            return false;
        }
        return true;
    }
    
}
