/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author UPID
 */
@Entity
public class UsuarioInfogov implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String nombre;
    private Long cuitl;
    
    private String contrasenia;

    public UsuarioInfogov() {
    }

    public UsuarioInfogov(String nombre, Long cuitl, String contrasenia) {
        this.nombre = nombre;
        this.cuitl = cuitl;
        this.contrasenia = contrasenia;
    }

    public Long getCuitl() {
        return cuitl;
    }

    public void setCuitl(Long cuitl) {
        this.cuitl = cuitl;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
    
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String id) {
        this.nombre = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioInfogov)) {
            return false;
        }
        UsuarioInfogov other = (UsuarioInfogov) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.UsuarioInfogov[ id=" + nombre + " ]";
    }
    
}
