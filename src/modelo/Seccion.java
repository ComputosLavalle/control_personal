
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Viktor
 */
public class Seccion {
    private int id;
    private String detalle;
    private int idArea;

    public Seccion() {
    }

    public Seccion(int id, String detalle, int idArea) {
        this.id = id;
        this.detalle = detalle;
        this.idArea = idArea;
    }

    public Seccion(String detalle, int idArea) {
        this.detalle = detalle;
        this.idArea = idArea;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    @Override
    public String toString() {
        return "Seccion{" + "id=" + id + ", detalle=" + detalle + ", idArea=" + idArea + '}';
    }
    
}
