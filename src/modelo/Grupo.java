/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Viktor
 */
@Entity
public class Grupo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private Time entrada;
    private Time salida;
    private boolean horaDeAlmuerzo;
    private boolean jornadaExtendida;

    public Grupo() {
    }

    public Grupo(String nombre, Time entrada, Time salida, boolean horaDeAlmuerzo, boolean jornadaExtendida) {
        this.nombre = nombre;
        this.entrada = entrada;
        this.salida = salida;
        this.horaDeAlmuerzo = horaDeAlmuerzo;
        this.jornadaExtendida =  jornadaExtendida;
    }
    public Grupo(int id, String nombre, Time entrada, Time salida, boolean horaDeAlmuerzo, boolean jornadaExtendida) {
        this.id = id;
        this.nombre = nombre;
        this.entrada = entrada;
        this.salida = salida;
        this.horaDeAlmuerzo = horaDeAlmuerzo;
        this.jornadaExtendida =  jornadaExtendida;
    }

    public boolean isHoraDeAlmuerzo() {
        return horaDeAlmuerzo;
    }

    public void setHoraDeAlmuerzo(boolean horaDeAlmuerzo) {
        this.horaDeAlmuerzo = horaDeAlmuerzo;
    }

    public boolean isJornadaExtendida() {
        return jornadaExtendida;
    }

    public void setJornadaExtendida(boolean jornadaExtendida) {
        this.jornadaExtendida = jornadaExtendida;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Time getEntrada() {
        return entrada;
    }

    public void setEntrada(Time entrada) {
        this.entrada = entrada;
    }

    public Time getSalida() {
        return salida;
    }

    public void setSalida(Time salida) {
        this.salida = salida;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Grupo{" + "id=" + id + ", nombre=" + nombre + ", entrada=" + entrada + ", salida=" + salida + ", horaDeAlmuerzo=" + horaDeAlmuerzo + ", jornadaExtendida=" + jornadaExtendida + '}';
    }
    
    
}
