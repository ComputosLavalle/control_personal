/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Viktor
 */
@Entity
public class PersonaGrupo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int userinfoid;
    private int grupo_id; 

    public PersonaGrupo() {
    }

    public PersonaGrupo(int id, int userinfoid, int grupo) {
        this.id = id;
        this.userinfoid = userinfoid;
        this.grupo_id = grupo;
    }
    
    public PersonaGrupo(int userinfoid, int grupo_id) {
        this.userinfoid = userinfoid;
        this.grupo_id = grupo_id;
    }

    public int getUserinfoid() {
        return userinfoid;
    }

    public void setUserinfoid(int userinfoid) {
        this.userinfoid = userinfoid;
    }

    public int getGrupo_id() {
        return grupo_id;
    }

    public void setGrupo_id(int grupo_id) {
        this.grupo_id = grupo_id;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaGrupo)) {
            return false;
        }
        PersonaGrupo other = (PersonaGrupo) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PersonaGrupo{" + "id=" + id + ", userinfoid=" + userinfoid + ", grupo_id=" + grupo_id + '}';
    }

    
    
}
