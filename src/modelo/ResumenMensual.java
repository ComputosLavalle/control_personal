/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.io.Serializable;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Viktor
 */
@Entity
public class ResumenMensual implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int id_persona;
    private Time horas_normales;
    private Time horas_extra;
    private Time horas_sabado_mañana;
    private Time horas_sabado_tarde;
    private Time horas_domingo_feriado;
    private int mes;

    public ResumenMensual() {
    }

    public ResumenMensual(int id_persona, Time horas_normales, Time horas_extra, Time horas_sabado_mañana, Time horas_sabado_tarde, Time horas_domingo_feriado, int mes) {
        this.id_persona = id_persona;
        this.horas_normales = horas_normales;
        this.horas_extra = horas_extra;
        this.horas_sabado_mañana = horas_sabado_mañana;
        this.horas_sabado_tarde = horas_sabado_tarde;
        this.horas_domingo_feriado = horas_domingo_feriado;
        this.mes = mes;
    }

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    public Time getHoras_normales() {
        return horas_normales;
    }

    public void setHoras_normales(Time horas_normales) {
        this.horas_normales = horas_normales;
    }

    public Time getHoras_extra() {
        return horas_extra;
    }

    public void setHoras_extra(Time horas_extra) {
        this.horas_extra = horas_extra;
    }

    public Time getHoras_sabado_mañana() {
        return horas_sabado_mañana;
    }

    public void setHoras_sabado_mañana(Time horas_sabado_mañana) {
        this.horas_sabado_mañana = horas_sabado_mañana;
    }

    public Time getHoras_sabado_tarde() {
        return horas_sabado_tarde;
    }

    public void setHoras_sabado_tarde(Time horas_sabado_tarde) {
        this.horas_sabado_tarde = horas_sabado_tarde;
    }

    public Time getHoras_domingo_feriado() {
        return horas_domingo_feriado;
    }

    public void setHoras_domingo_feriado(Time horas_domingo_feriado) {
        this.horas_domingo_feriado = horas_domingo_feriado;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResumenMensual)) {
            return false;
        }
        ResumenMensual other = (ResumenMensual) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ResumenMensual[ id=" + id + " ]";
    }
    
}
