/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo.optimizando;

/**
 *
 * @author fernando
 */
public class Usuario_OficinaOpt {
    
    private int id;
    private String id_usuario;
    
    private int id_oficina;

    public Usuario_OficinaOpt() {
    }

    public Usuario_OficinaOpt(String id_usuario, int id_oficina) {
        this.id_usuario = id_usuario;
        this.id_oficina = id_oficina;
    }

    public String getNombre() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getOficina() {
        return id_oficina;
    }

    public void setId_oficina(int id_oficina) {
        this.id_oficina = id_oficina;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}
