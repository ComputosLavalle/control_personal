/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.BDSentencias;
import controlador.ContFichada;
import controlador.ContLogger;
import controlador.ContPersonaGrupo;
import controlador.ContReloj;
import controlador.ContUserinfo;
import controlador.ContUsuario;
import controlador.fichadas.FichadaReloj2;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Fichada;
import modelo.Loggs;
import modelo.PersonaGrupo;
import modelo.Reloj;
import modelo.Userinfo;
import modelo.Usuario;
import modelo.optimizando.Usuario_NivelOpt;
import modelo.optimizando.Usuario_OficinaOpt;

/**
 *
 * @author Viktor
 */
public class V_principal extends javax.swing.JFrame {

    /**
     * Creates new form V_principal
     */
    static protected Usuario_OficinaOpt usuarioGlobal;
    private Usuario_NivelOpt usuario_NivelOpt;
    private Usuario usuarioSistema;
    private ContUsuario contUsuario;

    //CARGA DE FICHADAS
    List<Fichada> listaCargar;
    List<Userinfo> users_fails;
    File accesFile;
    private ContReloj contReloj = new ContReloj();
    private ContPersonaGrupo contPersonaGrupo = new ContPersonaGrupo();

    private ContUserinfo contUserinfo = new ContUserinfo();

    public V_principal() {
        contUsuario = new ContUsuario();
        //setExtendedState(MAXIMIZED_BOTH);
        initComponents();
        setIconImage(getIconImage());

        listaCargar = new ArrayList<>();
        users_fails = new ArrayList<>();

    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png"));

        return retValue;
    }

    private void otorgarPermisos() {
        switch (usuarioSistema.getTipo()) {
            case "admin":
                btn_bajar_datos.setEnabled(true);
                btn_horas.setEnabled(true);
                btn_licencias.setEnabled(true);
                btn_usuario.setEnabled(true);

                jMenu2.setEnabled(true);
                jMenu3.setEnabled(true);
                jMenu4.setEnabled(true);
                mni_gestion_empleados.setEnabled(true);

                mni_gestion_areas_secciones.setEnabled(true);
                break;
            case "supervisor":
                btn_bajar_datos.setEnabled(false);
                btn_horas.setEnabled(true);
                btn_licencias.setEnabled(true);
                btn_usuario.setEnabled(true);

                jMenu2.setEnabled(true);
                jMenu3.setEnabled(true);
                jMenu4.setEnabled(true);
                mni_gestion_empleados.setEnabled(true);

                mni_gestion_areas_secciones.setEnabled(true);

//                jMenuItem3.setEnabled(false);//aceso a seleccion de fichadas
                break;
            case "consulta":
                btn_bajar_datos.setEnabled(false);
                btn_horas.setEnabled(true);
                btn_licencias.setEnabled(false);

                jMenu2.setEnabled(true);
                jMenuItem7.setEnabled(true);
                jMenu3.setEnabled(false);
                jMenu4.setEnabled(false);
                mni_gestion_empleados.setEnabled(false);

                mni_gestion_areas_secciones.setEnabled(false);

                btn_usuario.setEnabled(false);
                break;
        }
    }

    private void deshabilitarTodo() {
        btn_bajar_datos.setEnabled(false);
        btn_horas.setEnabled(false);
        btn_licencias.setEnabled(false);

        jMenu2.setEnabled(false);
        jMenuItem7.setEnabled(false);
        jMenu3.setEnabled(false);
        jMenu4.setEnabled(false);
        mni_gestion_empleados.setEnabled(false);

        mni_gestion_areas_secciones.setEnabled(false);

        btn_usuario.setEnabled(false);
    }

    private void habilitarTodo() {
        btn_bajar_datos.setEnabled(true);
        btn_horas.setEnabled(true);
        btn_licencias.setEnabled(true);

        jMenu2.setEnabled(true);
        jMenuItem7.setEnabled(true);
        jMenu3.setEnabled(true);
        jMenu4.setEnabled(true);
        mni_gestion_empleados.setEnabled(true);

        mni_gestion_areas_secciones.setEnabled(true);

        btn_usuario.setEnabled(true);
    }

    public void almacenarUsuario(Usuario_OficinaOpt usuarioN) {

        this.usuarioGlobal = usuarioN;
        setTitle(getTitle() + " - " + usuarioGlobal.getNombre());
        usuarioSistema = contUsuario.encontrar(usuarioGlobal.getNombre());
        otorgarPermisos();
        iniciarCargaDeFichadas();

    }

    public void setUsuario_Nivel(Usuario_NivelOpt usuario_NivelOpt) {
        this.usuario_NivelOpt = usuario_NivelOpt;

    }

    private void iniciarCargaDeFichadas() {
        btn_bajar_datos.setEnabled(false);
        switch (usuarioSistema.getTipo()) {
            case "admin":
                //deshabilitarTodo();
                mni_cerrar_session.setEnabled(false);

                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        cargarDatosAcces();
                    }
                };
                Thread hilo = new Thread(r);
                hilo.start();

                break;
            default:
                barra.setVisible(false);
                lbl_cargando.setVisible(false);
                lbl_porcentaje.setVisible(false);
                break;
        }
    }

    private void buscarUsuariosNuevos(File file, List<Userinfo> listaTemp) {
        List<Userinfo> listaAux = new ArrayList<>();//contendar los usuarios de las acces
        int nuevosUsuariosNoVlaidos = 0;
        List<Userinfo> nuevosUsuarios = new ArrayList<>();//solo contendra los nuevos usuarios validos con nombre diferente de userid
        //Buscaremos los usuarios nuevos de att2000.mdb
        FichadaReloj2 f2 = new controlador.fichadas.FichadaReloj2(file.getAbsolutePath());
        listaAux = f2.traerEmpleados();

        int cont = 0;

        for (Userinfo u : listaAux) {
            boolean existe = false;
            for (Userinfo u1 : listaTemp) {

                if (Objects.equals(u.getUserid(), u1.getUserid())) {
                    existe = true;
                    break;
                } else {
                }
            }
            if (!existe) {
                if (!u.getName().equals(u.getUserid().toString())) {
                    System.out.println("Usuario NO existente valido: " + u.toString());
                    u.setTerminal(contUserinfo.getNombreTerminal());
                    u.setIp(contUserinfo.getIpTerminal());
                    u.setUsuariosistema(usuarioSistema.getUsuario());
                    
                    nuevosUsuarios.add(u);
                } else {
                    System.out.println("Usuario NO existente NO valido: " + u.toString());
                    nuevosUsuariosNoVlaidos++;
                    users_fails.add(u);
                }
            }
        }

        cont = cargarUsuariosNuevos(nuevosUsuarios, nuevosUsuariosNoVlaidos);

        if(cont > 0){
            JOptionPane.showMessageDialog(null, "Se han encontrado y cargado " + cont + " usuarios nuevos.");
        }
    }

    private int cargarUsuariosNuevos(List<Userinfo> nuevosUsuarios, int nuevosUsuariosNoVlaidos) {
        int cont = 0;
        if (nuevosUsuariosNoVlaidos > 0) {
            JOptionPane.showMessageDialog(null, "Tiene " + nuevosUsuariosNoVlaidos + " empleados sin un nombre idoneo.\n"
                    + "Estos empleados deben de darse de alta manualmente\n"
                    + "en el sistema aclarando nombre de cada uno. Usted de\n"
                    + "deberá hacerlo en la ventana que se encuentra en la\n"
                    + "siguiente direccion 'Grupo > Gestión de empleados'. \n"
                    + "Posteriormente se recomienda reiniciar el sistema o \n"
                    + "descargar las marcadas en el boton de la pantalla in-\n"
                    + "icial llamado 'Descargar fichadas'.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        //guardamos empleados
        for (Userinfo userinfo : nuevosUsuarios) {
            contUserinfo.create(userinfo);
            cont++;
        }

        //asignamos grupo horario
        for (Userinfo userinfo : nuevosUsuarios) {
            contPersonaGrupo.insert(
                    new PersonaGrupo(contUserinfo.encontrarUSERID(
                                    Integer.parseInt(userinfo.getUserid().toString())).getId(), 1));
        }

        return cont;
    }

    private void cargarDatosAcces() {

        //List<Reloj> listaRelojes = contReloj.encontrarTodos();
        File acces = new File(contReloj.traerPath());

        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        int cantidadReg = 0;
        
        lbl_porcentaje.setVisible(true);
        lbl_cargando.setVisible(true);
        barra.setVisible(true);
        barra.setValue(0);

        /**
         * ************** VERIFICAMOS SI EXISTEN USUARIOS NUEVOS
         * **********************
         */
        List<Userinfo> listaTemp = new ArrayList<>();

        for (Userinfo userinfo : contUserinfo.listar()) {
            listaTemp.add(userinfo);
        }

        buscarUsuariosNuevos(acces, listaTemp);
        /**
         * ******************************************************************************
         */


//            System.out.println("-----------------------------------------------------------");
            if (acces.getName().equals("att2000.mdb")) {
                FichadaReloj2 fichadaReloj2 = new FichadaReloj2(acces.getAbsolutePath());
                List<Fichada> listaF = new ArrayList<>();

                listaF = fichadaReloj2.trerFichadasTodos(usuarioSistema.getUsuario());
                //exportAExcel(listaF);
                cantidadReg = /*cantidadReg nunca se cambia*/cantidadReg + listaF.size();
                for (Fichada f : listaF) {
                    if (f != null) {
                        listaCargar.add(f);
                    }
                }
            }

        System.err.println("Tamaño total acces: " + cantidadReg);

        if (!listaCargar.isEmpty()) {
            ContFichada contFichada = new ContFichada();

            contFichada.ficharAloLoco(listaCargar, BDSentencias.conexionRelojLocal, acces, cantidadReg, users_fails);
            users_fails = new ArrayList<>();
        }
        System.out.println("FINALIZO LA CARGA");
        barra.setValue(100);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        if(!listaCargar.isEmpty()){
            JOptionPane.showMessageDialog(null, "Fichadas cargadas.");
        }
        lbl_porcentaje.setVisible(false);
        lbl_cargando.setVisible(false);
        barra.setVisible(false);
        
        btn_bajar_datos.setEnabled(true);
        
        habilitarTodo();
        mni_cerrar_session.setEnabled(true);

    }
    
    private void exportAExcel(List<Fichada> lista) {
        Calendar c = Calendar.getInstance();
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("D:/prueba-anterior"+c.get(Calendar.YEAR)+c.get(Calendar.MONTH)+c.get(Calendar.DAY_OF_MONTH)+c.get(Calendar.HOUR_OF_DAY)+c.get(Calendar.MINUTE)+c.get(Calendar.SECOND)+".csv");
            pw = new PrintWriter(fichero);

            for (Fichada f : lista) {
                pw.println(
                        f.getUserinfoid() + ";"
                        + f.getFecha() + ";"
                        + f.getHora() + ";"
                        + f.getReloj() + ";"
                        + f.getFechasubido() + ";"
                        + f.getTerminal() + ";"
                        + f.getIp() + ";"
                        + f.getObservacion() + ";"
                        + f.getUsuario());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        System.err.println("FICHERO 1 CREADO!");
    }
    public static void ingremetarLabel(String label){
        lbl_registros.setText(label);
    }
    public static void incrementoDeBarra(float valor) {
        barra.setValue((int) valor);
        lbl_porcentaje.setText(valor+"%");
        
        //efecto de los puntitos
        int p = 0;
        switch(p){
            case 0:
                lbl_cargando.setText("Cargando.");
                p++;
                break;
            case 1:
                lbl_cargando.setText("Cargando..");
                p++;
                break;
            case 2:
                lbl_cargando.setText("Cargando...");
                p = 0;
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_horas = new javax.swing.JButton();
        btn_bajar_datos = new javax.swing.JButton();
        btn_licencias = new javax.swing.JButton();
        btn_usuario = new javax.swing.JButton();
        barra = new javax.swing.JProgressBar();
        lbl_cargando = new javax.swing.JLabel();
        lbl_porcentaje = new javax.swing.JLabel();
        lbl_registros = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        mni_gestion_areas_secciones = new javax.swing.JMenuItem();
        mni_cerrar_session = new javax.swing.JMenuItem();
        mni_salir = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        mni_gestion_empleados = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema RelojMuni v1.18");

        jPanel1.setBackground(new java.awt.Color(153, 153, 153));

        btn_horas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/clock_128.png"))); // NOI18N
        btn_horas.setToolTipText("Ver marcadas");
        btn_horas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_horasActionPerformed(evt);
            }
        });

        btn_bajar_datos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Download-128.png"))); // NOI18N
        btn_bajar_datos.setToolTipText("Descargar fichadas.");
        btn_bajar_datos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_bajar_datosActionPerformed(evt);
            }
        });

        btn_licencias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/File-128.png"))); // NOI18N
        btn_licencias.setToolTipText("Licencias");
        btn_licencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_licenciasActionPerformed(evt);
            }
        });

        btn_usuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/personaGrande.png"))); // NOI18N
        btn_usuario.setToolTipText("Usuarios");
        btn_usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_usuarioActionPerformed(evt);
            }
        });

        lbl_cargando.setText("Cargando...");

        lbl_porcentaje.setText("0%");

        lbl_registros.setText("0 de 0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(barra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(btn_horas)
                        .addGap(18, 18, 18)
                        .addComponent(btn_bajar_datos)
                        .addGap(18, 18, 18)
                        .addComponent(btn_licencias)
                        .addGap(18, 18, 18)
                        .addComponent(btn_usuario)
                        .addGap(0, 165, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbl_cargando)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_porcentaje)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_registros)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_usuario)
                    .addComponent(btn_licencias)
                    .addComponent(btn_bajar_datos)
                    .addComponent(btn_horas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 249, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_cargando)
                    .addComponent(lbl_porcentaje)
                    .addComponent(lbl_registros))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(barra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jMenuBar1.setPreferredSize(new java.awt.Dimension(82, 50));

        jMenu5.setText("Sistema");
        jMenu5.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        mni_gestion_areas_secciones.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        mni_gestion_areas_secciones.setText("Gestión de Areas y secciones");
        mni_gestion_areas_secciones.setPreferredSize(new java.awt.Dimension(300, 40));
        mni_gestion_areas_secciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_gestion_areas_seccionesActionPerformed(evt);
            }
        });
        jMenu5.add(mni_gestion_areas_secciones);

        mni_cerrar_session.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        mni_cerrar_session.setText("Cerrar session");
        mni_cerrar_session.setPreferredSize(new java.awt.Dimension(250, 40));
        mni_cerrar_session.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_cerrar_sessionActionPerformed(evt);
            }
        });
        jMenu5.add(mni_cerrar_session);

        mni_salir.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        mni_salir.setText("Salir");
        mni_salir.setPreferredSize(new java.awt.Dimension(250, 40));
        mni_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_salirActionPerformed(evt);
            }
        });
        jMenu5.add(mni_salir);

        jMenuBar1.add(jMenu5);

        jMenu2.setText("Fichadas");
        jMenu2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jMenuItem2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem2.setText("Ver fichadas");
        jMenuItem2.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem3.setText("Múltiples fichadas");
        jMenuItem3.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem7.setText("Resumen de Horas");
        jMenuItem7.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        jMenu3.setText(" Empleado");
        jMenu3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jMenuItem4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem4.setText("Crear Grupo");
        jMenuItem4.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem5.setText("Horarios de empleado");
        jMenuItem5.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        mni_gestion_empleados.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        mni_gestion_empleados.setText(" Gestión de empleados");
        mni_gestion_empleados.setPreferredSize(new java.awt.Dimension(250, 40));
        mni_gestion_empleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_gestion_empleadosActionPerformed(evt);
            }
        });
        jMenu3.add(mni_gestion_empleados);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Opciones");
        jMenu4.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jMenuItem1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem1.setText("Gestion de Relojes");
        jMenuItem1.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem1);

        jMenuItem6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem6.setText("Licencias");
        jMenuItem6.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem6);

        jMenuItem8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jMenuItem8.setText(" Feriados");
        jMenuItem8.setPreferredSize(new java.awt.Dimension(250, 40));
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem8);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_seleccion_fichada(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_grupos(this, true).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_empleado_grupo(this, true).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_ver_fichada(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_licencias(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_bajar_resumen(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_feriados(this, true).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void btn_bajar_datosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_bajar_datosActionPerformed
//        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        new V_cagarDatos(this, true).setVisible(true);
//        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        iniciarCargaDeFichadas();
    }//GEN-LAST:event_btn_bajar_datosActionPerformed

    private void btn_horasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_horasActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        try {
            new V_ver_fichada(this, true, usuarioSistema).setVisible(true);
        } catch (Exception e) {

            ContLogger contLogger = new ContLogger();
            String error = "009";
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(V_principal.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    e.fillInStackTrace().toString(),
                    error);
            contLogger.insert(loggs);
            JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_horasActionPerformed

    private void btn_licenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_licenciasActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_licencias(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_licenciasActionPerformed

    private void btn_usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_usuarioActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_userinfo(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_usuarioActionPerformed

    private void mni_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_salirActionPerformed
        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }//GEN-LAST:event_mni_salirActionPerformed

    private void mni_cerrar_sessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_cerrar_sessionActionPerformed
        new SplashFrame().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_mni_cerrar_sessionActionPerformed

    private void mni_gestion_areas_seccionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_gestion_areas_seccionesActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_areas_secciones(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_mni_gestion_areas_seccionesActionPerformed

    private void mni_gestion_empleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_gestion_empleadosActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_userinfo(this, true, usuarioSistema).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_mni_gestion_empleadosActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        new V_relojes(this, true).setVisible(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(V_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(V_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(V_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(V_principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new V_principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    static javax.swing.JProgressBar barra;
    private javax.swing.JButton btn_bajar_datos;
    private javax.swing.JButton btn_horas;
    private javax.swing.JButton btn_licencias;
    private javax.swing.JButton btn_usuario;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JLabel lbl_cargando;
    public static javax.swing.JLabel lbl_porcentaje;
    public static javax.swing.JLabel lbl_registros;
    private javax.swing.JMenuItem mni_cerrar_session;
    private javax.swing.JMenuItem mni_gestion_areas_secciones;
    private javax.swing.JMenuItem mni_gestion_empleados;
    private javax.swing.JMenuItem mni_salir;
    // End of variables declaration//GEN-END:variables

}
