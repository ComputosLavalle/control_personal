/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.ContLogger;
import controlador.ContUsuarioInfogov;
import controlador.optimizando.ContUsuario_OficinaOpt;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import modelo.Loggs;
import modelo.optimizando.Usuario_OficinaOpt;

/**
 *
 * @author Viktor
 */
public class v_login extends javax.swing.JFrame {

    /**
     * Creates new form v_login
     */
    V_principal inicio;
    private SplashFrame splashFrame;

    private ContUsuarioInfogov contUsuarioInfogov;

    public v_login(SplashFrame splashFrame) {

        this.splashFrame = splashFrame;

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
//            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");

//            UIManager.setLookAndFeel("ch.randelshofer.quaqua.QuaquaLookAndFeel");
//            UIManager.setLookAndFeel(new SyntheticaBlackEyeLookAndFeel());
//            UIManager.setLookAndFeel(new SyntheticaBlackMoonLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaBlackStarLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaBlueIceLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaBlueMoonLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaBlueSteelLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaGreenDreamLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaMauveMetallicLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaOrangeMetallicLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaSilverMoonLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaSimple2DLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaSkyMetallicLookAndFeel());
//            //UIManager.setLookAndFeel(new SyntheticaWhiteVisionLookAndFeel());
//        } catch (ParseException ex) {
//            Logger.getLogger(v_login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(v_login.class.getName()).log(Level.SEVERE, null, ex);
            
            ContLogger contLogger = new ContLogger();
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    ex.fillInStackTrace().toString(),
                    "001");
            contLogger.insert(loggs);

            contLogger.insert(loggs);
        } catch (InstantiationException ex) {
            Logger.getLogger(v_login.class.getName()).log(Level.SEVERE, null, ex);
            ContLogger contLogger = new ContLogger();
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    ex.fillInStackTrace().toString(),
                    "002");
            contLogger.insert(loggs);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(v_login.class.getName()).log(Level.SEVERE, null, ex);
            ContLogger contLogger = new ContLogger();
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    ex.fillInStackTrace().toString(),
                    "003");
            contLogger.insert(loggs);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(v_login.class.getName()).log(Level.SEVERE, null, ex);
            ContLogger contLogger = new ContLogger();
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    ex.fillInStackTrace().toString(),
                    "004");
            contLogger.insert(loggs);
        }

        initComponents();

        setProgress(20, "Iniciando MUNIRELOJ");
        setProgress(22, "Iniciando MUNIRELOJ");
        setAlwaysOnTop(true);
        setProgress(25, "Iniciando MUNIRELOJ");
        setProgress(30, "Iniciando MUNIRELOJ");
        setProgress(35, "Iniciando MUNIRELOJ");
        setLocationRelativeTo(this);
        setProgress(40, "Iniciando MUNIRELOJ");
        setResizable(false);
        setProgress(43, "Gracias por su colaboración");
        contUsuarioInfogov = new ContUsuarioInfogov();
        setProgress(47, "Gracias por su colaboración");
        setProgress(53, "Bienvenidos a MUNIRELOJ");
        inicio = new V_principal();
        setProgress(60, "Bienvenidos a MUNIRELOJ");
        inicio.setEnabled(false);
        setProgress(65, "Bienvenidos a MUNIRELOJ");
        setProgress(75, "Bienvenidos a MUNIRELOJ");
        setProgress(85, "Bienvenidos a MUNIRELOJ");
        setProgress(95, "Bienvenidos a MUNIRELOJ");
        setIconImage(getIconImage());
    }

    @Override

    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png"));

        return retValue;
    }

    private void setProgress(int percent, String information) {
        splashFrame.getLabel().setText(information);

        splashFrame.getProgressBar().setValue(percent);

        /* 
         este Thread.sleep es solamente para que el Thread se duerma un momento 
         * cada vez que ingresamos un nuevo porcentaje al SplashScreen, de lo contrario 
         * la barra avanzaria demasiado rapido y no la veriamos, en una 
         * aplicacion real, esto no seria nesesario, ya que la aplicacion 
         * en verdad se tardaria al cargar acciones y metodos 
         */
        try {
            Thread.sleep(320);
        } catch (InterruptedException ex) {
            Logger.getLogger(v_login.class.getName()).log(Level.SEVERE, null, ex);
            ContLogger contLogger = new ContLogger();
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    ex.fillInStackTrace().toString(),
                    "005");
            contLogger.insert(loggs);
        }
    }
    
    private boolean verificar(int of,String user){
        boolean retornar = false;
        ContUsuario_OficinaOpt contUsuario_OficinaOpt = new ContUsuario_OficinaOpt();
        List <Usuario_OficinaOpt> usuarios = new ArrayList<>();
        usuarios = contUsuario_OficinaOpt.buscarPorUsuario(user);
        for (Usuario_OficinaOpt usuario : usuarios) {
            if (usuario.getOficina() == of) {
                retornar = true;
            }
        }
        return retornar;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_usuario = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        psw_contraseña = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 153, 153));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel2.setText("USUARIO:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel3.setText("CONTRASEÑA:");

        txt_usuario.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jButton1.setBackground(new java.awt.Color(204, 204, 204));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        jButton1.setText("INGRESAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        psw_contraseña.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        psw_contraseña.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                psw_contraseñaKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                psw_contraseñaKeyTyped(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/msn.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_usuario, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                    .addComponent(psw_contraseña)))
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(29, 29, 29))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 20, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txt_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(psw_contraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        String respuesta = "";
        int habilitado = 0;
        try {
            habilitado = contUsuarioInfogov.buscarUsuarioInfoGovPorNombre(txt_usuario.getText());
            if (habilitado == 1) {
                this.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El usuario se encuentra inhabilitado\nComunicarse con el Área de Cómputos");
                this.setAlwaysOnTop(true);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.setAlwaysOnTop(false);
            System.out.println("Problemas al validar usuario " + e.getMessage());
            
            ContLogger contLogger = new ContLogger();
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    e.fillInStackTrace().toString(),
                    "006");
            contLogger.insert(loggs);
            
            this.setAlwaysOnTop(true);
        }
        try {
            respuesta = contUsuarioInfogov.fnValidarUsuario(txt_usuario.getText());
            switch (respuesta) {
                case "El Usuario NO EXISTE...":
                    this.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, respuesta);
                    this.setAlwaysOnTop(true);
                    txt_usuario.setText("");
                    txt_usuario.requestFocus();
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.setAlwaysOnTop(false);
            System.out.println("Problemas al validar usuario " + e.getMessage());
            
            ContLogger contLogger = new ContLogger();
            String error = "007";
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    e.fillInStackTrace().toString(),
                    error);
            contLogger.insert(loggs);
            
            
            this.setAlwaysOnTop(true);
        }

        try {

            respuesta = contUsuarioInfogov.fnValidarClaveUsuario(txt_usuario.getText(), psw_contraseña.getText());
            switch (respuesta) {
                case "La clave es incorrecta...":
                    this.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, respuesta);
                    this.setAlwaysOnTop(true);

                    break;
                case "El Usuario tiene la clave básica 123456...":
                    this.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "Comunicarse con el Área de Cómputos para gestionar su clave", "ERROR", JOptionPane.ERROR_MESSAGE);
                    this.setAlwaysOnTop(true);

                    break;

                case "":
                    String user = txt_usuario.getText();
//                    CDFS
                    
                    
                    if (verificar(409,user) && user.equals("vperez")) {
                        inicio.setEnabled(true);
                        inicio.setVisible(true);
                        this.dispose();
                    }else{
                        this.setVisible(false);
                        JOptionPane.showMessageDialog(null, "Usuario denegado","",JOptionPane.ERROR_MESSAGE);
                        this.setVisible(true);
                    }
            }
        } catch (Exception e) {
            this.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Comunicarse con el Área de Cómputos");
            
            ContLogger contLogger = new ContLogger();
            String error = "008";
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(v_login.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    e.fillInStackTrace().toString(),
                    error);
            contLogger.insert(loggs);
            
            this.setAlwaysOnTop(true);
            System.out.println("problema al comparar usuario y contraseña" + e.getMessage());
        }
//         List <Usuario_oficina> listaUsuarioInfoGov = new ArrayList<>();
//         listaUsuarioInfoGov = contUsuarioOficina.buscarPorUsuario(txt_usuario.getText());
//        principal.almacenarUsuario(listaUsuarioInfoGov.get(0));
//        principal.setEnabled(true);
//this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void psw_contraseñaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_psw_contraseñaKeyTyped

    }//GEN-LAST:event_psw_contraseñaKeyTyped

    private void psw_contraseñaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_psw_contraseñaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButton1ActionPerformed(null);
        }
    }//GEN-LAST:event_psw_contraseñaKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(v_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(v_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(v_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(v_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new v_login(null).setVisible();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPasswordField psw_contraseña;
    private javax.swing.JTextField txt_usuario;
    // End of variables declaration//GEN-END:variables
}
