/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relojmuni;

import controlador.BDSentencias;
import controlador.ContFichada;
import controlador.ContLogger;
import controlador.ContPersonaGrupo;
import controlador.ContUsuario;
import controlador.ControladorGeneral;
import controlador.fichadas.FichadaReloj1;
import controlador.fichadas.FichadaReloj2;
import controlador.fichadas.Utilidades;
import controlador.optimizando.ContUsuario_OficinaOpt;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Array;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JFileChooser;
import modelo.Fichada;
import modelo.Usuario;
import modelo.UsuarioInfogov;
import modelo.optimizando.Usuario_OficinaOpt;

/**
 *
 * @author Viktor
 */
public class RelojMuni {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

//        SimpleDateFormat sdf = new SimpleDateFormat("01:30:20");
//        System.out.println(sdf.format(new Date()));
//        
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("relojMuniPU");
//        EntityManager em = emf.createEntityManager();
//        FichadaReloj1 fichadaReloj1 = new FichadaReloj1();
//        ControladorGeneral controladorGeneral = new ControladorGeneral();
//        controladorGeneral.ficharAloLoco(fichadaReloj1.trerFichadasTodos(), BDSentencias.conexionRelojLocal);
//        FichadaReloj2 fichadaReloj2 = new FichadaReloj2();
//        fichadaReloj2.trerFichadasTodos("2014-03-18", "2014-03-20");
//        FichadaReloj1 fichadaReloj1 = new FichadaReloj1();
//        fichadaReloj1.trerFichadasTodos("2014-03-10", "2014-03-14");
        //EL CODIGO ACONTINUACION GENERA EL TOTAL DE LAS HORAS DE TRABAJADAS POR UN EMPLEADO
//        ContFichada contFichada = new ContFichada();
//        Object[] parametros = {"20131490209"};
//        List<List> lista =contFichada.buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.fichadaMes, parametros, BDSentencias.conexionRelojLocal);
//   
//        List<Fichada> listaFichadas = new ArrayList<>();
//        for (List list : lista) {
//            Fichada fichada = contFichada.setearObjeto(list);
//            listaFichadas.add(fichada);
//        }
//        
//        String fechaInicial = "";
//        Long primeraMarcada = null;
//        Long segundaMarcada = null;
//        Long dia = null;
//        Long total = null;
//        int marca = 0;
//        
//        for (int i = 0; i < listaFichadas.size(); i++) {
//            String fechaAux = listaFichadas.get(i).getFechaHora().toString().split(" ")[0];
//            
//            if (fechaInicial.equals(fechaAux)) {
//                
//                if (marca == 1) {
//                    segundaMarcada = listaFichadas.get(i).getFechaHora().getTime();
//                    
//                    dia = segundaMarcada - primeraMarcada;
//                    System.out.println(fechaAux+": horas : " + (dia+20000)/3600000 + " : " + dia%3600000/60000);
//                 marca = 0;
//                }else{
//                    primeraMarcada = listaFichadas.get(i).getFechaHora().getTime();
//                    marca = 1;
//                }
//                
//                
//            }else{
//                
//                fechaInicial = fechaAux;
//                primeraMarcada = listaFichadas.get(i).getFechaHora().getTime();
//                marca = 1;
//            }
//            
//            
//            
//        }
//        
//        for (Fichada fichada : listaFichadas) {
//            String fechaAux = fichada.getFechaHora().toString().split(" ")[0];
//            if (fechaInicial.equals(fechaAux)) {
//                
//            }else{
//                fechaInicial = fechaAux;
//                
//            }
//        }
        // HACE UN INSERT DE CADA UNO DE LOS EMPLEADOS VINCULANDOLOS CON EL HORARIO DE 7:30 A 13:30 LLAMADO "horario1"
//        ControladorGeneral c = new ControladorGeneral();
//        
//        String [] col = {"CUCUPERS"};
//        String sql = "SELECT DISTINCT CUCUPERS FROM fichada";
//        
//        List list = c.listarTodo(col, sql, BDSentencias.conexionRelojLocal);
//        
//        
//        String sql2 = "INSERT INTO personagrupo (ID_PERSONA, GRUPO_ID) VALUES (?,?)";
//        for (Iterator it = list.iterator(); it.hasNext();) {
//            Object object = it.next();
//            
//                Object[] datos = new Object[2];
//                datos[0] = object;
//                datos[1] = 1;
//                c.ejecutarSentencia(datos, sql2, BDSentencias.conexionRelojLocal);
//            
//            
//        }
//        System.out.println("TERMINO LA INSERCION!");
//        ContPersonaGrupo c = new ContPersonaGrupo();
//        c.getIdGrupo(Long.parseLong("20131490209"));
//        Calendar c = Calendar.getInstance();
//        c.setTimeInMillis(Timestamp.valueOf("2014-05-04 03:18:00").getTime());
//        System.out.println(c.get(Calendar.DAY_OF_WEEK));
        //Creamos selector de apertura
//        JFileChooser chooser = new JFileChooser();
//        chooser.setCurrentDirectory(new java.io.File("."));
////Titulo que llevara la ventana
//        chooser.setDialogTitle("Titulo");
////Elegiremos archivos del directorio
//        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
//        chooser.setAcceptAllFileFilterUsed(false);
////Si seleccionamos algún archivo retornaremos su directorio
//        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
//            System.out.println("Directorio: " + chooser.getCurrentDirectory());
////Si no seleccionamos nada retornaremos No seleccion
//        } else {
//            System.out.println("No seleccion ");
//        }
//        ControladorGeneral cg = new ControladorGeneral();
//        String[] col = {"FECHA", "DETALLE"};
//        
//        List<Object> l = cg.listarTodo(col, "SELECT * FROM feriado", BDSentencias.conexionRelojLocal);
//        
//        for (Object list : l) {
//            
//            System.out.println(list.toString());
//        }
//        Utilidades u = new Utilidades();
//        System.out.println(u.obtenerDiaDeSemana(Date.valueOf("2014-06-01")));
//        Timestamp hoy = new Timestamp(Calendar.getInstance().getTimeInMillis());
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println("Fecha de hoy: "+df.format(hoy));
//        List<Usuario> l = new ContUsuario().encontrarTodos();
//        for (Usuario l1 : l) {
//            System.out.println(l1.toString());
//        }
//        Calendar c = GregorianCalendar.getInstance();
//        System.out.println("fecha: "+c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DAY_OF_MONTH));
//        ContLogger c = new ContLogger();
//        try {
//            c.crearLog();
//        } catch (IOException ex) {
//            Logger.getLogger(RelojMuni.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
        try {
            String thisIp = InetAddress.getLocalHost()
                    .getHostAddress();
            System.out.println("IP:" + thisIp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
