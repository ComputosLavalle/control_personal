/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Viktor
 */
public class detalleEmpleado {
    private String nombre;
    private String cuil;
    private String reloj;
    private String horario;
    private String feriados;
    private String licencias;
    private String periodo;
    
    private String normales;
    private String  extras;
    private String retrazo;
    private String sabadoM;
    private String sabadoT;
    private String domingo;
    private String feriado;
    private List<Marcadas> listaMarcadas;

    public detalleEmpleado() {
    }

    public detalleEmpleado(String nombre, String cuil, String reloj, String horario, String feriados, String licencias, String normales, String extras, String retrazo, String sabadoM, String sabadoT, String domingo, String feriado) {
        this.nombre = nombre;
        this.cuil = cuil;
        this.reloj = reloj;
        this.horario = horario;
        this.feriados = feriados;
        this.licencias = licencias;
        this.normales = normales;
        this.extras = extras;
        this.retrazo = retrazo;
        this.sabadoM = sabadoM;
        this.sabadoT = sabadoT;
        this.domingo = domingo;
        this.feriado = feriado;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public List<Marcadas> getListaMarcadas() {
        return listaMarcadas;
    }

    public void setListaMarcadas(List<Marcadas> listaMarcadas) {
        this.listaMarcadas = listaMarcadas;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCuil() {
        return cuil;
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public String getReloj() {
        return reloj;
    }

    public void setReloj(String reloj) {
        this.reloj = reloj;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getFeriados() {
        return feriados;
    }

    public void setFeriados(String feriados) {
        this.feriados = feriados;
    }

    public String getLicencias() {
        return licencias;
    }

    public void setLicencias(String licencias) {
        this.licencias = licencias;
    }

    public String getNormales() {
        return normales;
    }

    public void setNormales(String normales) {
        this.normales = normales;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getRetrazo() {
        return retrazo;
    }

    public void setRetrazo(String retrazo) {
        this.retrazo = retrazo;
    }

    public String getSabadoM() {
        return sabadoM;
    }

    public void setSabadoM(String sabadoM) {
        this.sabadoM = sabadoM;
    }

    public String getSabadoT() {
        return sabadoT;
    }

    public void setSabadoT(String sabadoT) {
        this.sabadoT = sabadoT;
    }

    public String getDomingo() {
        return domingo;
    }

    public void setDomingo(String domingo) {
        this.domingo = domingo;
    }

    public String getFeriado() {
        return feriado;
    }

    public void setFeriado(String feriado) {
        this.feriado = feriado;
    }
    
    

    public List getMarcada()   
    {       
        return listaMarcadas;   
    }    

    public void setMarcadas(List marcadas)   
    {       
        this.listaMarcadas = marcadas;   
    }    

    public void addMarcada(Marcadas materia)   
    {       
        this.listaMarcadas.add(materia);   
    }    
  
    public JRDataSource getMarcadas()   
    {       
        return new JRBeanCollectionDataSource(listaMarcadas);   
    } 
}
