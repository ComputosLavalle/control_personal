/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Viktor
 */
public class Marcadas {
    public String dia;
    public String fecha;
    public String entrada1;
    public String salida1;
    public String entrada2;
    public String salida2;
    public String entrada3;
    public String salida3;
    public String normales;
    public String extras;
    public String retrazo;

    public Marcadas() {
    }

    public Marcadas(String fecha, String entrada1, String salida1, String entrada2, String salida2, String entrada3, String salida3, String normales, String extras, String retrazo,String dia) {
        this.fecha = fecha;
        this.entrada1 = entrada1;
        this.salida1 = salida1;
        this.entrada2 = entrada2;
        this.salida2 = salida2;
        this.entrada3 = entrada3;
        this.salida3 = salida3;
        this.normales = normales;
        this.extras = extras;
        this.retrazo = retrazo;
        this.dia = dia;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEntrada1() {
        return entrada1;
    }

    public void setEntrada1(String entrada1) {
        this.entrada1 = entrada1;
    }

    public String getSalida1() {
        return salida1;
    }

    public void setSalida1(String salida1) {
        this.salida1 = salida1;
    }

    public String getEntrada2() {
        return entrada2;
    }

    public void setEntrada2(String entrada2) {
        this.entrada2 = entrada2;
    }

    public String getSalida2() {
        return salida2;
    }

    public void setSalida2(String salida2) {
        this.salida2 = salida2;
    }

    public String getEntrada3() {
        return entrada3;
    }

    public void setEntrada3(String entrada3) {
        this.entrada3 = entrada3;
    }

    public String getSalida3() {
        return salida3;
    }

    public void setSalida3(String salida3) {
        this.salida3 = salida3;
    }

    public String getNormales() {
        return normales;
    }

    public void setNormales(String normales) {
        this.normales = normales;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getRetrazo() {
        return retrazo;
    }

    public void setRetrazo(String retrazo) {
        this.retrazo = retrazo;
    }
}
