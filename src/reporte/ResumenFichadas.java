/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package reporte;

/**
 *
 * @author Viktor
 */
public class ResumenFichadas {
    String nombre;
    String hs_normales;
    String hs_extras;
    String hs_retrazo;
    String inconcluso;
    String licencia;

    public ResumenFichadas() {
    }

    public ResumenFichadas(String nombre, String hs_normales, String hs_extras, String hs_retrazo, String inconcluso, String licencia) {
        this.nombre = nombre;
        this.hs_normales = hs_normales;
        this.hs_extras = hs_extras;
        this.hs_retrazo = hs_retrazo;
        this.inconcluso = inconcluso;
        this.licencia = licencia;
    }
    
    public String getInconcluso() {
        return inconcluso;
    }

    public void setInconcluso(String inconcluso) {
        this.inconcluso = inconcluso;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHs_normales() {
        return hs_normales;
    }

    public void setHs_normales(String hs_normales) {
        this.hs_normales = hs_normales;
    }

    public String getHs_extras() {
        return hs_extras;
    }

    public void setHs_extras(String hs_extras) {
        this.hs_extras = hs_extras;
    }

    public String getHs_retrazo() {
        return hs_retrazo;
    }

    public void setHs_retrazo(String hs_retrazo) {
        this.hs_retrazo = hs_retrazo;
    }
    
}
